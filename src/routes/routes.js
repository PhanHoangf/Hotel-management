import React from 'react'
import { Route } from 'react-router'
import NotfoundPage from '../pages/not-found/NotfoundPage'
import LoginPage from '../pages/login/LoginPage'
import RegisterPage from '../pages/register/RegisterPage'
import Wrap from '../pages/wrap/Wrap'
import LandingPage from '../pages/landing/LandingPage'
import PrivateRoute from '../components/auth/PrivateRoute'
const Routes = {}

Routes.routes = [
    {
        path: '/',
        exact: true,
        main: () => <LandingPage />,
    },
    {
        path: '/main',
        // exact: true,
        main: () => <Wrap />,
    },
    {
        path: '/login',
        main: () => <LoginPage />,
    },
    {
        path: '/register',
        main: () => <RegisterPage />,
    },
    {
        path: '/404',
        main: () => <NotfoundPage />,
    },
]

Routes.routesConfig = (routes) => {
    let result = routes.map((route, index) => {
        if (route.path === '/main') {
            return (
                <PrivateRoute key={index} path={route.path}>
                    {route.main()}
                </PrivateRoute>
            )
        }
        return <Route key={index} path={route.path} exact={route.exact} component={route.main} />
    })
    return result
}

export default Routes
