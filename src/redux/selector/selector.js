import { createSelector } from 'reselect'
import { getJSDocReturnType } from 'typescript'

const roomTypeSelector = (state) => state.roomType

export const mappedRoomType = createSelector(roomTypeSelector, (roomTypes) => {
    return roomTypes.map((entity) => {
        return {
            id: entity._id,
            nameType: entity.nameType,
        }
    })
})

const roomsSelector = (state) => state.room

// export const mappedRoom = createSelector(roomSelector, (rooms) => {
//     return rooms.map((entity) => {
//         return {
//             id: entity._id,
//             isClean: entity.isClean,
//             typeRoom: entity.typeRoom
//         }
//     })
// })

export const mappedCleanRoomSelector = createSelector(roomsSelector, (rooms) => {
    return rooms.filter((entity) => {
        return entity.isClean === true
    })
})
