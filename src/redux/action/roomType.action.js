import { roomTypeApi } from '../../apis/roomtype.api'
import { CREATE_ROOM_TYPE, DELETE_ROOM_TYPE, GET_ROOM_TYPE, UPDATE_ROOM_TYPE } from '../actionType'

export const getRoomType = (roomTypes) => {
    return {
        type: GET_ROOM_TYPE,
        payload: {
            data: roomTypes,
        },
    }
}

export const createRoomType = (roomType) => {
    return {
        type: CREATE_ROOM_TYPE,
        payload: {
            data: roomType,
        },
    }
}

export const updateRoomType = (id, roomType) => {
    return {
        type: UPDATE_ROOM_TYPE,
        payload: {
            id,
            data: roomType,
        },
    }
}

export const deleteRoomType = (id) => {
    return {
        type: DELETE_ROOM_TYPE,
        payload: {
            data: id,
        },
    }
}

export const getRoomTypeRequest = () => {
    return async (dispatch) => {
        try {
            const response = await roomTypeApi.getAllRoomType()
            dispatch(getRoomType(response.data.data))
        } catch (error) {
            console.log(error)
        }
    }
}

export const createRoomTypeRequest = (roomType) => {
    return async (dispatch) => {
        try {
            const response = await roomTypeApi.createRoomType(roomType)
            console.log(response.data.data)
            dispatch(createRoomType(response.data.data))
        } catch (error) {
            console.log(error)
        }
    }
}

export const deleteRoomTypeRequest = (id) => {
    return async (dispatch) => {
        try {
            const response = await roomTypeApi.deleteRoomType(id)
            if (response) dispatch(deleteRoomType(id))
        } catch (error) {
            console.log(error)
        }
    }
}

export const updateRoomTypeRequest = (id, roomType) => {
    return async (dispatch) => {
        try {
            const response = await roomTypeApi.updateRoomType(id, roomType)
            dispatch(updateRoomType(id, response.data.data))
        } catch (error) {
            console.log(error)
        }
    }
}
