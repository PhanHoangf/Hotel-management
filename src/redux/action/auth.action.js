import { userApi } from '../../apis/user.api'
import { SIGN_IN, SIGN_OUT } from '../actionType'

export const signIn = () => {
    return {
        type: SIGN_IN,
    }
}

export const signOut = () => {
    return {
        type: SIGN_OUT,
    }
}

export const signInRequest = (candidateUser, cb) => {
    return async (dispatch) => {
        try {
            const response = await userApi.login(candidateUser)
            if (response.data.token) {
                userApi.setToken(response.data.token)
                dispatch(signIn())
                cb()
            }
        } catch (error) {
            console.log(error)
            alert('Wrong password or username')
        }
    }
}
