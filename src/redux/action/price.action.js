import { priceApi } from '../../apis/price.api'
import { CREATE_PRICE, DELETE_PRICE, GET_PRICES, UPDATE_PRICE } from '../actionType'

export const getPrices = (data) => {
    return {
        type: GET_PRICES,
        payload: {
            data,
        },
    }
}

export const createPrice = (data) => {
    return {
        type: CREATE_PRICE,
        payload: {
            data,
        },
    }
}

export const getPricesRequest = () => {
    return async (dispatch) => {
        try {
            const response = await priceApi.getAllPrices()
            dispatch(getPrices(response.data.data))
        } catch (error) {
            console.log(error.status)
        }
    }
}

export const createPriceRequest = (data) => {
    return async (dispatch) => {
        try {
            const response = await priceApi.createPrice(data)
            console.log(response)
            if (response) dispatch(createPrice(response.data.data))
        } catch (error) {
            console.log(error)
        }
    }
}

export const deletePrice = (id) => {
    return {
        type: DELETE_PRICE,
        payload: {
            id,
        },
    }
}

export const deletePriceRequest = (id) => {
    return async (dispatch) => {
        try {
            const response = await priceApi.deletePrice(id)
            console.log(response)
            dispatch(deletePrice(id))
        } catch (error) {
            console.log(error)
        }
    }
}

const updatePrice = (id, data) => {
    return {
        type: UPDATE_PRICE,
        payload: {
            id,
            data,
        },
    }
}

export const updatePriceRequest = (id, data) => {
    return async (dispatch) => {
        try {
            const response = await priceApi.updatePrice(id, data)
            if (response) dispatch(updatePrice(id, data))
        } catch (error) {
            alert(error.message)
        }
    }
}
