import { roomApi } from '../../apis/room.api'
import { CLEAN_ROOM, CREATE_ROOM, DELETE_ROOM, GET_ROOMS, UPDATE_ROOM } from '../actionType'

export const getRooms = (data) => {
    return {
        type: GET_ROOMS,
        payload: {
            data,
        },
    }
}

export const createRoom = (data) => {
    return {
        type: CREATE_ROOM,
        payload: {
            data,
        },
    }
}

export const deleteRoom = (id) => {
    return {
        type: DELETE_ROOM,
        payload: {
            id,
        },
    }
}

export const updateRoom = (id, data) => {
    return {
        type: UPDATE_ROOM,
        payload: {
            id,
            data,
        },
    }
}

export const cleanRoom = (id, data) => {
    return {
        type: CLEAN_ROOM,
        payload: {
            id,
            data,
        },
    }
}

export const getRoomsRequest = () => {
    return async (dispatch) => {
        try {
            const response = await roomApi.getAllRooms()
            dispatch(getRooms(response.data.data))
        } catch (error) {
            console.log(error)
        }
    }
}

export const createRoomRequest = (room) => {
    return async (dispatch) => {
        try {
            const response = await roomApi.createRoom(room)
            console.log(response.data.data)
            dispatch(createRoom(response.data.data))
        } catch (error) {
            console.log(error)
        }
    }
}

export const deleteRoomRequest = (id) => {
    return async (dispatch) => {
        try {
            const response = await roomApi.deleteRoom(id)
            console.log(response)
            if (response.data.success) dispatch(deleteRoom(id))
        } catch (error) {
            console.log(error)
        }
    }
}

export const updateRoomRequest = (id, body) => {
    return async (dispatch) => {
        try {
            const response = await roomApi.updateRoom(id, body)
            // console.log(response.data.data)
            if (response.data.success) dispatch(updateRoom(id, response.data.data))
        } catch (error) {
            console.log(error)
        }
    }
}

export const cleanRoomRequest = (id, data, cb) => {
    return async (dispatch) => {
        try {
            const response = await roomApi.cleanRoom(id, data)
            dispatch(cleanRoom(id, data))
            if (typeof cb === 'function') {
                cb()
            }
        } catch (error) {
            console.log(error)
            alert(ERROR_MESSAGE.FAILED)
        }
    }
}

const ERROR_MESSAGE = {
    FAILED: 'Something wrong, try again later!!',
}
