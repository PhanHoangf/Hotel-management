import { GET_BOOKING_ROOMS } from '../actionType'
import { bookingRoomApi } from '../../apis/bookingRoom.api'

export const getBookingRooms = (data) => {
    return {
        type: GET_BOOKING_ROOMS,
        payload: {
            data,
        },
    }
}

export const getBookingRoomsRequest = () => {
    return async (dispatch) => {
        try {
            const response = await bookingRoomApi.getAllBookingRooms()
            if (response) dispatch(getBookingRooms(response.data.data))
        } catch (error) {
            console.log(error)
        }
    }
}
