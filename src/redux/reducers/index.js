import { combineReducers } from 'redux'
import authReducer from './auth.reducer'
import bookingRoomReducer from './bookingRoom.reducer'
import priceReducer from './price.reducer'
import roomReducer from './room.reducer'
import roomTypeReducer from './roomType.reducer'

export default combineReducers({
    auth: authReducer,
    roomType: roomTypeReducer,
    room: roomReducer,
    price: priceReducer,
    bookingRoom: bookingRoomReducer,
})
