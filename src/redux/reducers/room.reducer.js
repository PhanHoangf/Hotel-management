import { CLEAN_ROOM, CREATE_ROOM, DELETE_ROOM, GET_ROOMS, UPDATE_ROOM } from '../actionType'

const INITIAL_STATE = []

export default function roomReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case GET_ROOMS: {
            return action.payload.data
        }
        case CREATE_ROOM: {
            const newState = [...state]
            newState.push(action.payload.data)
            return newState
        }
        case DELETE_ROOM: {
            return [...state].filter((entity) => entity._id !== action.payload.id)
        }
        case UPDATE_ROOM: {
            const newState = [...state]
            const index = state.findIndex((entity) => entity._id === action.payload.id)
            newState[index] = { ...newState[index], ...action.payload.data }
            return newState
        }
        case CLEAN_ROOM: {
            const newState = [...state]
            const index = newState.findIndex((entity) => entity._id === action.payload.id)
            newState[index] = { ...newState[index], isClean: action.payload.data.isClean }
            return newState
        }
        default: {
            return state
        }
    }
}
