const {
    GET_ROOM_TYPE,
    CREATE_ROOM_TYPE,
    DELETE_ROOM_TYPE,
    UPDATE_ROOM_TYPE,
} = require('../actionType')

const INITIAL_STATE = []

function roomTypeReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case GET_ROOM_TYPE: {
            const newState = action.payload.data
            return newState
        }
        case CREATE_ROOM_TYPE: {
            const newState = [...state, action.payload.data]
            return newState
        }
        case DELETE_ROOM_TYPE: {
            return [...state].filter((entity) => entity._id !== action.payload.data)
            // return newState.filter((entity) => entity._id === action.payload.data)
        }
        case UPDATE_ROOM_TYPE: {
            const index = state.findIndex((entity) => {
                return entity._id === action.payload.data._id
            })
            const newState = [...state]
            newState[index] = { ...newState[index], ...action.payload.data }
            return newState
        }
        default:
            return state
    }
}

export default roomTypeReducer
