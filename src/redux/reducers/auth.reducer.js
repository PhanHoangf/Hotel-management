const { SIGN_IN, SIGN_OUT } = require('../actionType')

const token = localStorage.getItem('token')

const INITIAL_STATE = {
    isAuthenticated: token ? true : false,
}

function authReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SIGN_IN:
            return { ...state, isAuthenticated: true }
        case SIGN_OUT:
            return { ...state, isAuthenticated: false }
        default:
            return state
    }
}

export default authReducer
