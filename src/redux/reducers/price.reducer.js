import { CREATE_PRICE, DELETE_PRICE, GET_PRICES, UPDATE_PRICE } from '../actionType'

const INITIAL_STATE = []

export default function priceReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case GET_PRICES: {
            return action.payload.data
        }
        case CREATE_PRICE: {
            const newState = [...state]
            newState.push(action.payload.data)
            return newState
        }
        case UPDATE_PRICE: {
            const newState = [...state]
            const index = newState.findIndex((entity) => entity._id === action.payload.id)
            if (index === -1) return newState
            newState[index] = { ...newState[index], ...action.payload.data }
            return newState
        }
        case DELETE_PRICE: {
            const newState = [...state].filter((price) => price._id !== action.payload.id)
            return newState
        }
        default:
            return state
    }
}
