import { GET_BOOKING_ROOMS } from '../actionType'

export default function bookingRoomReducer(state = [], action) {
    switch (action.type) {
        case GET_BOOKING_ROOMS: {
            return action.payload.data
        }
        default:
            return state
    }
}
