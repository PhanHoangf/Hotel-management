import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Link } from 'react-router-dom'
import { LANDING_PAGE_CONST as HeaderItems } from '../../constants/LandingPage.const'
import { Container, Grid } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}))

export default function LandingPage() {
    const classes = useStyles()
    const navBarItems = HeaderItems.map((items) => {
        return (
            <div style={{ marginRight: '25px' }}>
                <Link to={`${items.link}`}>{items.title}</Link>
            </div>
        )
    })
    return (
        <Container>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        Hotel Management
                    </Typography>
                    {navBarItems}
                </Toolbar>
            </AppBar>

            <div style={{ marginLeft: '500px' }}>Welcome to my page</div>
        </Container>
    )
}
