import {
    Card,
    CardContent,
    Divider,
    Grid,
    IconButton,
    Menu,
    MenuItem,
    Typography
} from '@material-ui/core'
import CheckIcon from '@material-ui/icons/Check'
import ClearIcon from '@material-ui/icons/Clear'
import LoyaltyIcon from '@material-ui/icons/Loyalty'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import React, { useState } from 'react'
import { connect } from 'react-redux'
import { cleanRoomRequest, getRoomsRequest } from '../../../redux/action/rooms.action'
import { getRoomTypeRequest } from '../../../redux/action/roomType.action'
import './check-in-room-card.scss'

function CheckInRoomCard(props) {
    const [anchorEl, setAnchorEl] = useState(null)

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    const onCleaned = (id, isClean) => {
        props.setLoading(true)
        props.cleanRoomRequest(id, { isClean }, () => {
            props.setLoading(false)
        })
        handleClose()
    }

    return (
        <div className="check-in-room-card-container">
            <Card>
                <CardContent style={{ padding: '0px' }}>
                    <div className="room-card-header display-flex">
                        <Typography
                            variant="subtitle1"
                            style={{ color: '#21A0A3', marginRight: '10px' }}
                        >
                            {props.room.nameRoom}
                        </Typography>
                        {props.room.isClean ? (
                            <LoyaltyIcon fontSize="small" style={{ color: 'red' }} />
                        ) : (
                            ''
                        )}
                        <IconButton
                            aria-controls="simple-menu"
                            aria-haspopup="true"
                            onClick={handleClick}
                            style={{ outline: 'none' }}
                            size="small"
                        >
                            <MoreVertIcon />
                        </IconButton>
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem>
                                <Grid container spacing={3}>
                                    <Grid item xs={4}>
                                        <VpnKeyIcon />
                                    </Grid>
                                    <Grid item xs={8}>
                                        Check-in
                                    </Grid>
                                </Grid>
                            </MenuItem>
                            <MenuItem
                                onClick={() => onCleaned(props.room._id, !props.room.isClean)}
                                className="menu-item"
                                disabled={props.room.isClean ? true : false}
                            >
                                <Grid container spacing={3}>
                                    <Grid item xs={4}>
                                        <LoyaltyIcon fontSize="small" />
                                    </Grid>
                                    <Grid item xs={8}>
                                        Cleaned
                                    </Grid>
                                </Grid>
                            </MenuItem>
                        </Menu>
                    </div>
                    <Divider />
                    <div className="room-card-body display-flex">
                        {props.room.isClean ? (
                            <ClearIcon style={{ marginRight: '10px', color: 'red' }} />
                        ) : (
                            <CheckIcon style={{ marginRight: '10px', color: '#5DAF50' }} />
                        )}
                        <Typography variant="subtitle1" className="mr-auto">
                            Is cleaned
                        </Typography>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        rooms: state.room,
        roomType: state.roomType,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRoomsRequest: () => {
            dispatch(getRoomsRequest())
        },
        getRoomTypeRequest: () => {
            dispatch(getRoomTypeRequest())
        },
        cleanRoomRequest: (id, data, cb) => {
            dispatch(cleanRoomRequest(id, data, cb))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckInRoomCard)
