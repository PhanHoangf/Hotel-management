import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Grid,
    makeStyles,
    Typography
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import React from 'react'
import { connect } from 'react-redux'
import CustomBackdrop from '../../../components/back-drop/CustomBackdrop'
import { getRoomsRequest } from '../../../redux/action/rooms.action'
// import { getRoomsRequest } from '../../../redux/action/rooms.action'
import { getRoomTypeRequest } from '../../../redux/action/roomType.action'
import { mappedRoomType } from '../../../redux/selector/selector'
import CheckInRoomCard from '../room-card/CheckInRoomCard'

const useStyles = makeStyles(() => ({
    root: {
        background: 'transparent',
        '&:hover': {
            background: '#F5F5F5',
        },
    },
}))

class WaittingRoomController extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
        }
        this.setLoading = this.setLoading.bind(this)
    }

    componentDidMount() {
        this.props.getRoomTypeRequest()
        this.props.getRoomRequest()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.roomTypes !== this.props.roomTypes) {
            this.setLoading(false)
        }
    }

    setLoading(value) {
        this.setState({
            isLoading: value,
        })
    }

    get roomTypes() {
        return this.props.roomTypes
    }

    get rooms() {
        return this.props.rooms
    }
}

class WaitingRoom extends WaittingRoomController {
    render() {
        return (
            <div>
                {this.roomTypes.map((entity) => {
                    return (
                        <React.Fragment>
                            <Accordion
                                key={entity.id}
                                style={{
                                    background: 'transparent',
                                }}
                            >
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography>{entity.nameType}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2}>
                                        {this.rooms.map((room) => {
                                            if (room.typeRoom._id === entity.id) {
                                                return (
                                                    <Grid item>
                                                        <CheckInRoomCard
                                                            key={room._id}
                                                            room={room}
                                                            setLoading={this.setLoading}
                                                        />
                                                    </Grid>
                                                )
                                            }
                                        })}
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                        </React.Fragment>
                    )
                })}
                <CustomBackdrop open={this.state.isLoading} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        roomTypes: mappedRoomType(state),
        rooms: state.room,
    }
}

const mapDispathToProps = (dispatch) => {
    return {
        getRoomTypeRequest: () => {
            dispatch(getRoomTypeRequest())
        },
        getRoomRequest: () => {
            dispatch(getRoomsRequest())
        },
    }
}

export default connect(mapStateToProps, mapDispathToProps)(WaitingRoom, WaittingRoomController)
