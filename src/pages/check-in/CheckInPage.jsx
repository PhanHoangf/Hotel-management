import React, { Suspense, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Container,
    Divider,
    Grid,
    TextField,
} from '@material-ui/core'

import MeetingRoomIcon from '@material-ui/icons/MeetingRoom'
import NoMeetingRoomIcon from '@material-ui/icons/NoMeetingRoom'
import LocalLaundryServiceIcon from '@material-ui/icons/LocalLaundryService'
import HistoryIcon from '@material-ui/icons/History'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import WaitingRoom from './waiting-room/WaitingRoom'
import CleanRoom from './clean-room/CleanRoom'

// const WaitingRoom = React.lazy(() => import('./waiting-room/WaitingRoom'))

function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3} style={{ padding: '0' }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
        height: `112.5vh`,
        padding: '0',
    },
    scheduleContainer: {
        display: 'flex',
        width: '100%',
        height: '100%',
        marginLeft: '-50px',
    },
    scheduleControl: {
        width: '65%',
        backgroundColor: 'transparent',
    },
    transparent: {
        background: 'transparent',
        '&:hover': {
            background: '#F5F5F5',
        },
    },
}))

function CheckInPage() {
    const [value, setValue] = useState(0)
    const handleChange = (event, newValue) => {
        setValue(newValue)
    }
    const classes = useStyles()
    return (
        <Container maxWidth="xl" className={classes.root}>
            <div className={classes.scheduleContainer}>
                <div className={classes.scheduleControl}>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="fullWidth"
                        indicatorColor="primary"
                        aria-label="icon tabs example"
                    >
                        <Tab
                            icon={<MeetingRoomIcon style={{ color: ' #66C0B8' }} />}
                            label="Waiting Room"
                            style={{ outline: 'none' }}
                        />
                        <Tab
                            icon={<NoMeetingRoomIcon style={{ color: '#FF9A7A' }} />}
                            label="Hired Room"
                            style={{ outline: 'none' }}
                        />
                        <Tab
                            icon={<LocalLaundryServiceIcon style={{ color: '#F89189' }} />}
                            label="Cleaning Room"
                            style={{ outline: 'none' }}
                        />
                        <Tab
                            icon={<HistoryIcon style={{ color: '#A8D8AA' }} />}
                            label="History"
                            style={{ outline: 'none' }}
                        />
                    </Tabs>
                    <Divider></Divider>
                    <TabPanel value={value} index={0} style={{ padding: '0 0 0 24px' }}>
                        <Suspense fallback={<div>Loading...</div>}>
                            <WaitingRoom />
                        </Suspense>
                    </TabPanel>
                    <TabPanel value={value} index={2} style={{ padding: '0 0 0 24px' }}>
                        <CleanRoom />
                    </TabPanel>
                </div>
                <Divider orientation="vertical"></Divider>
                <div></div>
            </div>
        </Container>
    )
}

export default CheckInPage
