import {
    Card,
    CardContent,
    Divider,
    Grid,
    IconButton,
    Menu,
    MenuItem,
    Tooltip,
    Typography,
} from '@material-ui/core'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import HourglassEmptyIcon from '@material-ui/icons/HourglassEmpty'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import React from 'react'
import { connect } from 'react-redux'
import { cleanRoomRequest } from '../../../../redux/action/rooms.action'
import '../../room-card/check-in-room-card.scss'

class CleanRoomCardController extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            anchorEl: null,
        }
        this.setAncorEl = this.setAncorEl.bind(this)
        this.handleClick = this.handleClick.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.onCleaned = this.onCleaned.bind(this)
    }

    setAncorEl(value) {
        this.setState({
            anchorEl: value,
        })
    }

    handleClick(event) {
        this.setAncorEl(event.currentTarget)
    }

    handleClose() {
        this.setAncorEl(null)
    }

    onCleaned(id, isClean) {
        this.props.setLoading(true)
        this.props.cleanRoomRequest(id, { isClean }, () => {
            this.props.setLoading(false)
        })
        this.handleClose()
    }
}

class CleanRoomCard extends CleanRoomCardController {
    render() {
        const { room } = this.props
        return (
            <div className="check-in-room-card-container">
                <Card>
                    <CardContent style={{ padding: '0px' }}>
                        <div className="room-card-header display-flex">
                            <Typography
                                variant="subtitle1"
                                style={{ color: '#F44336', marginRight: '10px' }}
                            >
                                {room.nameRoom}
                            </Typography>
                            <IconButton
                                aria-controls="simple-menu"
                                aria-haspopup="true"
                                onClick={this.handleClick}
                                style={{ outline: 'none' }}
                                size="small"
                            >
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="simple-menu"
                                anchorEl={this.state.anchorEl}
                                keepMounted
                                open={Boolean(this.state.anchorEl)}
                                onClose={this.handleClose}
                            >
                                <MenuItem
                                    onClick={() => this.onCleaned(room._id, !room.isClean)}
                                    className="menu-item"
                                >
                                    <Grid container spacing={3}>
                                        <Grid item xs={4}>
                                            <CheckCircleIcon fontSize="small" />
                                        </Grid>
                                        <Grid item xs={8}>
                                            Cleaned
                                        </Grid>
                                    </Grid>
                                </MenuItem>
                            </Menu>
                        </div>
                        <Divider />
                        <div className="room-card-body display-flex">
                            <Tooltip title="room status">
                                <HourglassEmptyIcon fontSize="small" className="mr-20" />
                            </Tooltip>
                            {room.isUsed ? (
                                <Typography variant="subtitle2" className="mr-auto text-red">
                                    Used room
                                </Typography>
                            ) : (
                                <Typography variant="subtitle2" className="mr-auto text-green">
                                    Watting room
                                </Typography>
                            )}
                        </div>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        cleanRoomRequest: (id, data, cb) => {
            dispatch(cleanRoomRequest(id, data, cb))
        },
    }
}

export default connect(null, mapDispatchToProps)(CleanRoomCard)
