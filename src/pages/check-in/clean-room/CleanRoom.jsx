import { Accordion, AccordionDetails, AccordionSummary, Grid, Typography } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import React from 'react'
import { connect } from 'react-redux'
import CustomBackdrop from '../../../components/back-drop/CustomBackdrop'
import { getRoomsRequest } from '../../../redux/action/rooms.action'
import { getRoomTypeRequest } from '../../../redux/action/roomType.action'
import { mappedCleanRoomSelector, mappedRoomType } from '../../../redux/selector/selector'
import CheckInRoomCard from '../room-card/CheckInRoomCard'
import CleanRoomCard from './clean-room-card/CleanRoomCard'

class CleanRoomController extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
        }
        this.setLoading = this.setLoading.bind(this)
    }

    // componentDidMount() {
    //     if (this.props.cleanRooms.length === 0) {
    //         this.props.getRoomRequest()
    //     }
    //     if (this.props.roomTypes.length === 0) {
    //         this.props.getRoomTypeRequest()
    //     }
    // }

    // componentDidUpdate(prevProps) {
    //     if (
    //         prevProps.roomTypes !== this.props.roomTypes ||
    //         prevProps.cleanRooms !== this.props.cleanRooms
    //     ) {
    //         this.setLoading(false)
    //     }
    // }

    setLoading(value) {
        this.setState({
            isLoading: value,
        })
    }

    get cleanRooms() {
        return this.props.cleanRooms
    }

    get roomTypes() {
        return this.props.roomTypes
    }
}

class CleanRoom extends CleanRoomController {
    render() {
        return (
            <div>
                {this.roomTypes.map((entity) => {
                    return (
                        <React.Fragment>
                            <Accordion
                                key={entity.id}
                                style={{
                                    background: 'transparent',
                                }}
                            >
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography>{entity.nameType}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Grid container spacing={2}>
                                        {this.cleanRooms.map((room) => {
                                            if (room.typeRoom._id === entity.id) {
                                                return (
                                                    <Grid item>
                                                        <CleanRoomCard
                                                            key={room._id}
                                                            room={room}
                                                            setLoading={this.setLoading}
                                                        />
                                                    </Grid>
                                                )
                                            }
                                        })}
                                    </Grid>
                                </AccordionDetails>
                            </Accordion>
                        </React.Fragment>
                    )
                })}
                <CustomBackdrop open={this.state.isLoading} />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cleanRooms: mappedCleanRoomSelector(state),
        roomTypes: mappedRoomType(state),
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRoomTypeRequest: () => {
            dispatch(getRoomTypeRequest())
        },
        getRoomRequest: () => {
            dispatch(getRoomsRequest())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CleanRoom, CleanRoomController)
