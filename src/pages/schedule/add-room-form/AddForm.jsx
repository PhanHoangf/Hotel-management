import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'
import SaveIcon from '@material-ui/icons/Save'
import { Button, Divider, Grid, TextField, Tooltip } from '@material-ui/core'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import { addDays } from 'date-fns'
import DateFnsUtils from '@date-io/date-fns'
import { makeStyles } from '@material-ui/core/styles'
import AddIcon from '@material-ui/icons/Add'
import SystemUpdateAltIcon from '@material-ui/icons/SystemUpdateAlt'
import './addform.scss'
import ClearIcon from '@material-ui/icons/Clear'

const useStyles = makeStyles((theme) => ({
    addControl: {},
}))

function AddRoomController() {
    const [singleRoom, setSingleRoom] = useState(0)
    const [doubleRoom, setDoubleRoom] = useState(0)

    function onEditSingleRoom(number) {
        if (number < 0 && singleRoom == 0) return
        else setSingleRoom(singleRoom + number)
    }

    function onEditDoubleRoom(number) {
        if (number < 0 && doubleRoom == 0) return
        else setDoubleRoom(doubleRoom + number)
    }

    function total() {
        return singleRoom + doubleRoom
    }

    return {
        singleRoom,
        doubleRoom,
        onEditDoubleRoom,
        onEditSingleRoom,
        total,
    }
}

function AddRoomForm(props) {
    const [fromDate, setFromdDate] = useState(new Date())
    const [toDate, setToDate] = useState(addDays(fromDate, 1))
    const controller = AddRoomController()

    const handleFromDateChange = (date) => {
        setFromdDate(date)
    }

    const handleToDateChange = (date) => {
        setToDate(date)
    }

    const onCloseForm = () => {
        props.onSetVisible(false)
    }

    return (
        <div className="add-room-form">
            <form className="add-room-container">
                {!props.visible ? (
                    <Button
                        color="primary"
                        variant="outlined"
                        startIcon={<AddIcon />}
                        style={{ outline: 'none', marginLeft: '10px' }}
                        onClick={() => props.onSetVisible(true)}
                    >
                        Add
                    </Button>
                ) : (
                    <React.Fragment>
                        <div className="form-header pl-10">
                            <Typography variant="h6" style={{ textTransform: 'uppercase' }}>
                                Adding
                            </Typography>
                            <Button
                                color="primary"
                                startIcon={<SaveIcon />}
                                className="ml-auto"
                                // onClick={() => props.onSetVisible(true)}
                            >
                                Save
                            </Button>
                            <Button
                                color="secondary"
                                startIcon={<ClearIcon />}
                                style={{ outline: 'none' }}
                                onClick={onCloseForm}
                            >
                                Cancel
                            </Button>
                        </div>
                        <hr />
                        <div className="save-room-body">
                            <Grid container justify="flex-start" spacing={3} alignItems="center">
                                <Grid item xs={12}>
                                    <TextField id="standard-basic" label="Guest name" fullWidth />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField id="standard-basic" label="Phone number" fullWidth />
                                </Grid>
                            </Grid>
                            <br></br>
                            <br></br>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <Grid
                                    container
                                    justify="flex-start"
                                    spacing={3}
                                    alignItems="center"
                                >
                                    <Grid item xs={6}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            variant="inline"
                                            label="From"
                                            value={fromDate}
                                            onChange={handleFromDateChange}
                                            minDate={new Date()}
                                            maxDate={toDate}
                                            format="MM/dd/yyyy"
                                            size="small"
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <KeyboardDatePicker
                                            disableToolbar
                                            variant="inline"
                                            label="To"
                                            value={toDate}
                                            onChange={handleToDateChange}
                                            minDate={new Date()}
                                            format="MM/dd/yyyy"
                                            size="small"
                                            fullWidth
                                        />
                                    </Grid>
                                </Grid>
                            </MuiPickersUtilsProvider>
                            <Grid container justify="flex-start" spacing={3} alignItems="center">
                                <Grid item xs={12}>
                                    <TextField id="standard-basic" label="Deposit" fullWidth />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField id="standard-basic" label="Deals" fullWidth />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField id="standard-basic" label="Note" fullWidth  />
                                </Grid>
                            </Grid>
                        </div>
                        <Divider />
                        <div className="form-header pl-10">
                            <Typography variant="subtitle1">Booking room</Typography>
                            <Button
                                color="secondary"
                                startIcon={<AddIcon />}
                                style={{ outline: 'none' }}
                            >
                                Add room
                            </Button>
                        </div>
                        <Divider />
                        <div className="add-room-body">
                            <h6 className="text-center">Not specified room</h6>
                            <Divider />
                            <div className="add-room-action pl-10">
                                <Typography variant="subtitle1">Room type booking</Typography>
                                <Button
                                    color="primary"
                                    startIcon={<SystemUpdateAltIcon />}
                                    style={{ outline: 'none' }}
                                >
                                    Updated by room
                                </Button>
                            </div>
                            <Divider />
                            <div className="pl-10 mt-8 mb-8 form-rows">
                                <h6>Room type (+1)</h6>
                                <h6>Room number (-1)</h6>
                            </div>
                            <Divider />
                            <div className="pl-10 mt-8 mb-8 form-rows">
                                <Tooltip title="click +1">
                                    <h6
                                        className="click-able "
                                        onClick={() => controller.onEditSingleRoom(1)}
                                    >
                                        Single room
                                    </h6>
                                </Tooltip>
                                <Tooltip title="click -1">
                                    <h6
                                        className="click-able "
                                        onClick={() => controller.onEditSingleRoom(-1)}
                                    >
                                        {controller.singleRoom}
                                    </h6>
                                </Tooltip>
                            </div>
                            <Divider />
                            <div className="pl-10 mt-8 mb-8 form-rows">
                                <Tooltip
                                    title="click +1"
                                    onClick={() => controller.onEditDoubleRoom(1)}
                                >
                                    <h6 className="click-able ">Double room</h6>
                                </Tooltip>
                                <Tooltip
                                    title="click -1"
                                    onClick={() => controller.onEditDoubleRoom(-1)}
                                >
                                    <h6 className="click-able ">{controller.doubleRoom}</h6>
                                </Tooltip>
                            </div>
                            <Divider />
                            <div className="pl-10 mt-8 mb-8 form-rows">
                                <h6>Total</h6>
                                <h6>{controller.singleRoom + controller.doubleRoom}</h6>
                            </div>
                            <Divider />
                        </div>
                    </React.Fragment>
                )}
            </form>
        </div>
    )
}

export default AddRoomForm
