import { createSelector } from 'reselect'
import { getDate } from '../../../helpers/function'
const roomTypeScheduleSelector = (state) => state

export const mappedRoomTypeSchedule = createSelector(roomTypeScheduleSelector, (bookingRooms) =>
    bookingRooms.map((entity) => {
        const newStartDate = getDate(entity.dateReceive)
        const newEndDate = getDate(entity.dateCheckout)
        return {
            title: entity.nameCustomer,
            startDate: new Date(newStartDate.year, newStartDate.month, newStartDate.day).toLocaleDateString(),
            endDate: new Date(newEndDate.year, newEndDate.month, newEndDate.day).toLocaleDateString(),
        }
    })
)
