import React, { useEffect } from 'react'
import { MonthView } from '@devexpress/dx-react-scheduler-material-ui'
// import { MonthView } from '@devexpress/dx-react-scheduler';
import { Scheduler, DayView, Appointments } from '@devexpress/dx-react-scheduler-material-ui'
import { ViewState } from '@devexpress/dx-react-scheduler'
import { DateNavigator } from '@devexpress/dx-react-scheduler-material-ui'
import { Toolbar } from '@devexpress/dx-react-scheduler-material-ui'
import Paper from '@material-ui/core/Paper'
import './room-type-schedule.scss'
import { connect } from 'react-redux'
import { getBookingRoomsRequest } from '../../../redux/action/bookingRoom.action'
import { mappedRoomTypeSchedule } from './room-type-schedule.selector'

const currentDate = new Date()

function RoomTypeSchedule(props) {

    useEffect(() => {
        props.getBookingRoomsRequest()
    }, [])

    console.log(props.bookingRoom)
    return (
        <div className="room-type-schedule-container ">
            <Paper>
                <Scheduler data={props.bookingRoom}>
                    <ViewState defaultCurrentDate={currentDate} />
                    <MonthView />
                    <Toolbar />
                    <DateNavigator />
                    <Appointments />
                </Scheduler>
            </Paper>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        bookingRoom: mappedRoomTypeSchedule(state.bookingRoom),
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getBookingRoomsRequest: () => {
            dispatch(getBookingRoomsRequest())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomTypeSchedule)
