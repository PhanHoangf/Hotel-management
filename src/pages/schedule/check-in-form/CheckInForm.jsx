import React from 'react'
import './check-in-form.scss'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import CheckIcon from '@material-ui/icons/Check'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import ClearIcon from '@material-ui/icons/Clear'
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Backdrop,
    Button,
    CircularProgress,
    Divider,
    makeStyles,
    Typography,
} from '@material-ui/core'
import { connect } from 'react-redux'
import { mappedRoomTypeSelector } from '../../setting/setting-room/add-room/add-room.selector'
import { getRoomTypeRequest } from '../../../redux/action/roomType.action'
import RoomCard from '../room-card/RoomCard'

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#3F51B5',
    },
}))

class CheckInForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
        }
    }

    componentDidMount() {
        this.props.getRoomTypeRequest()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.typeRoom !== this.props.typeRoom) {
            this.setState({
                isLoading: false,
            })
        }
    }

    render() {
        const roomTypes = this.props.typeRoom
        return (
            <div className="check-in-form-container">
                <div className="form-header pl-10">
                    <VpnKeyIcon color="secondary" className="mr-10" />
                    <Typography variant="subtitle1">SELECT</Typography>
                    <Button
                        color="primary"
                        startIcon={<CheckIcon />}
                        className="ml-auto"
                        style={{ outline: 'none' }}
                    >
                        Check-in
                    </Button>
                    <Button
                        color="secondary"
                        onClick={() => this.props.onSetCheckInForm(false)}
                        startIcon={<ClearIcon />}
                        style={{ outline: 'none' }}
                    >
                        Canel
                    </Button>
                </div>
                <Divider />
                <div>
                    {roomTypes.map((entity) => {
                        return (
                            <Accordion key={entity._id} style={{ background: 'transparent' }}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography variant="subtitle1">{entity.nameType}</Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <RoomCard />
                                </AccordionDetails>
                            </Accordion>
                        )
                    })}
                </div>
                <Backdrop open={this.state.isLoading} style={{ color: '#3F51B5', zIndex: '1' }}>
                    <CircularProgress color="inherit" />
                </Backdrop>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        typeRoom: mappedRoomTypeSelector(state.roomType),
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRoomTypeRequest: () => {
            dispatch(getRoomTypeRequest())
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckInForm)
