import { createSelector } from 'reselect'

const typeRoomSelector = (state) => state

const mappedTypeRoomSelector = createSelector(typeRoomSelector, (typeRooms) =>
    typeRooms.map((entity) => {
        return {
            nameType: entity.nameType,
        }
    })
)
