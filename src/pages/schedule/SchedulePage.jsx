import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import { Container, Divider, Grid, TextField } from '@material-ui/core'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import { addDays } from 'date-fns'
import DateFnsUtils from '@date-io/date-fns'
import HotelIcon from '@material-ui/icons/Hotel'
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation'
import DateRangeIcon from '@material-ui/icons/DateRange'
import SearchIcon from '@material-ui/icons/Search'
import RoomTypeSchedule from './roomtype/RoomTypeSchedule'
import AddRoomForm from './add-room-form/AddForm'
import ViewByDay from './view-by-day/ViewByDay'
import CheckInForm from './check-in-form/CheckInForm'

function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-force-tabpanel-${index}`}
            aria-labelledby={`scrollable-force-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
        height: `112.5vh`,
        paddingTop: '0',
        paddingBottom: '0',
    },
    scheduleContainer: {
        display: 'flex',
        width: '100%',
        height: '100%',
        marginLeft: '-50px',
    },
    scheduleControl: {
        width: '65%',
        backgroundColor: 'transparent',
    },
}))

// function a11yProps(index) {
//     return {
//         id: `scrollable-prevent-tab-${index}`,
//         'aria-controls': `scrollable-prevent-tabpanel-${index}`,
//     }
// }

function SchedulePage(props) {
    const classes = useStyles()
    const [value, setValue] = useState(0)
    const [fromDate, setFromdDate] = useState(new Date())
    const [toDate, setToDate] = useState(addDays(fromDate, 1))
    const [isVisible, setVisible] = useState(false)
    const [isCheckInFormOpen, setCheckInForm] = useState(false)

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    const onSetVisible = (visible) => {
        setVisible(visible)
    }

    const handleFromDateChange = (date) => {
        setFromdDate(date)
    }

    const handleToDateChange = (date) => {
        setToDate(date)
    }

    const onSetCheckInForm = (isOpen) => {
        setCheckInForm(isOpen)
    }
    return (
        <Container maxWidth="xl" className={classes.root}>
            <div className={classes.scheduleContainer}>
                <div className={classes.scheduleControl}>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        variant="fullWidth"
                        indicatorColor="primary"
                        textColor="primary"
                        aria-label="icon tabs example"
                    >
                        <Tab
                            icon={<HotelIcon />}
                            label="Room schedule"
                            style={{ outline: 'none' }}
                        />
                        <Tab
                            icon={<DateRangeIcon />}
                            label="Room type schedule"
                            style={{ outline: 'none' }}
                        />
                        <Tab
                            icon={<InsertInvitationIcon />}
                            label="View by day"
                            style={{ outline: 'none' }}
                        />
                        <Tab icon={<SearchIcon />} label="Search" style={{ outline: 'none' }} />
                    </Tabs>
                    <Divider></Divider>
                    <TabPanel value={value} index={0}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Grid container justify="flex-start" spacing={2} alignItems="center">
                                <Grid item xs={3}>
                                    <KeyboardDatePicker
                                        disableToolbar
                                        variant="inline"
                                        label="From"
                                        value={fromDate}
                                        onChange={handleFromDateChange}
                                        minDate={new Date()}
                                        maxDate={toDate}
                                        format="MM/dd/yyyy"
                                        size="small"
                                    />
                                </Grid>
                                <Grid item xs={3}>
                                    <KeyboardDatePicker
                                        disableToolbar
                                        variant="inline"
                                        label="To"
                                        value={toDate}
                                        onChange={handleToDateChange}
                                        minDate={new Date()}
                                        format="MM/dd/yyyy"
                                        size="small"
                                    />
                                </Grid>
                                <Grid item xs={3}>
                                    <TextField id="standard-basic" label="Room type" />
                                </Grid>
                                <Grid item xs={3}>
                                    <TextField id="standard-basic" label="Room name" />
                                </Grid>
                            </Grid>
                        </MuiPickersUtilsProvider>
                        <Grid container justify="flex-start" spacing={2} alignItems="center">
                            <Grid item>
                                <Grid container spacing={1} alignItems="center">
                                    <Grid item style={{ background: 'red' }}></Grid>
                                    <Grid item>
                                        <Typography variant="subtitle2">No check-in</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Grid container spacing={1} alignItems="center">
                                    <Grid item style={{ background: 'green' }}></Grid>
                                    <Grid item>
                                        <Typography variant="subtitle2">Check-in</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Grid container spacing={1} alignItems="center">
                                    <Grid item style={{ background: 'yellow' }}></Grid>
                                    <Grid item>
                                        <Typography variant="subtitle2">Check-out</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <RoomTypeSchedule />
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        <ViewByDay onSetCheckInForm={onSetCheckInForm} />
                    </TabPanel>
                </div>
                <Divider orientation="vertical"></Divider>
                {isCheckInFormOpen ? (
                    <CheckInForm onSetCheckInForm={onSetCheckInForm} />
                ) : (
                    <AddRoomForm onSetVisible={onSetVisible} visible={isVisible} />
                )}
            </div>
        </Container>
    )
}

export default SchedulePage
