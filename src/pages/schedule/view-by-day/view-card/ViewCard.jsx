import {
    Card,
    CardContent,
    CardHeader,
    Divider,
    Grid,
    IconButton,
    Menu,
    MenuItem,
    Paper,
    Tooltip,
    Typography,
} from '@material-ui/core'
import CreateIcon from '@material-ui/icons/Create'
import FaceIcon from '@material-ui/icons/Face'
import SwapHorizIcon from '@material-ui/icons/SwapHoriz'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import TimerIcon from '@material-ui/icons/Timer'
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom'
import PhoneIcon from '@material-ui/icons/Phone'
import HotelIcon from '@material-ui/icons/Hotel'
import CancelIcon from '@material-ui/icons/Cancel'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import DeleteIcon from '@material-ui/icons/Delete'
import React from 'react'
import './view-card.scss'

function ViewCard(props) {
    const [anchorEl, setAnchorEl] = React.useState(null)

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    const setCheckInForm = () => {
        props.onSetCheckInForm(true)
        handleClose()
    }
    return (
        <div className="card-view-day-container">
            <Card variant="outlined">
                <CardHeader
                    subheader="#1"
                    action={
                        <div>
                            <IconButton
                                aria-controls="simple-menu"
                                aria-haspopup="true"
                                onClick={handleClick}
                                style={{ outline: 'none' }}
                            >
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="simple-menu"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                            >
                                <MenuItem onClick={handleClose}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={4}>
                                            <CreateIcon />
                                        </Grid>
                                        <Grid item xs={8}>
                                            Modify
                                        </Grid>
                                    </Grid>
                                </MenuItem>
                                <MenuItem onClick={setCheckInForm}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={4}>
                                            <VpnKeyIcon />
                                        </Grid>
                                        <Grid item xs={8}>
                                            Check-in
                                        </Grid>
                                    </Grid>
                                </MenuItem>
                                <MenuItem onClick={handleClose}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={4}>
                                            <CancelIcon />
                                        </Grid>
                                        <Grid item xs={8}>
                                            Cancel
                                        </Grid>
                                    </Grid>
                                </MenuItem>
                                <MenuItem onClick={handleClose}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={4}>
                                            <DeleteIcon />
                                        </Grid>
                                        <Grid item xs={8}>
                                            Delete
                                        </Grid>
                                    </Grid>
                                </MenuItem>
                            </Menu>
                        </div>
                    }
                />
                <Divider />
                <CardContent>
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <Tooltip title="Customer">
                                <FaceIcon />
                            </Tooltip>
                        </Grid>
                        <Grid item xs={8}>
                            Hoang
                        </Grid>
                        <Grid item xs={4}>
                            <Tooltip title="Status">
                                <SwapHorizIcon />
                            </Tooltip>
                        </Grid>
                        <Grid item xs={8}>
                            Not check-in
                        </Grid>
                        <Grid item xs={4}>
                            <Tooltip title="Duration">
                                <TimerIcon />
                            </Tooltip>
                        </Grid>
                        <Grid item xs={8}>
                            4/12 - 5/12
                        </Grid>
                        <Grid item xs={2}>
                            <Tooltip title="Rooms">
                                <MeetingRoomIcon />
                            </Tooltip>
                        </Grid>
                        <Grid item xs={2}>
                            1
                        </Grid>
                        <Grid item xs={2}>
                            <Tooltip title="Phone number">
                                <PhoneIcon />
                            </Tooltip>
                        </Grid>
                        <Grid item xs={2}>
                            0769845228
                        </Grid>
                        <Grid item xs={12}>
                            <Grid item xs={4}>
                                <Tooltip title="Room list">
                                    <HotelIcon />
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </div>
    )
}

export default ViewCard
