import { Divider, Grid, IconButton, Typography } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import React, { useState } from 'react'
import DateFnsUtils from '@date-io/date-fns'
import './view-by-day.scss'
import ViewCard from './view-card/ViewCard'

function ViewByDay(props) {
    const [fromDate, setFromDate] = useState(new Date())
    // const [toDate, setToDate] = useState(addDays(fromDate, 1))

    const handleFromDateChange = (date) => {
        setFromDate(date)
    }

    return (
        <div className="view-by-day-container">
            <Grid container spacing={3} style={{ padding: '0px', borderBottom: '1px solid black' }}>
                <Grid item>
                    <IconButton aria-label="delete" style={{ outline: 'none' }}>
                        <ArrowBackIcon />
                    </IconButton>
                </Grid>
                <Grid item>
                    <IconButton aria-label="delete" style={{ outline: 'none' }}>
                        <ArrowForwardIcon />
                    </IconButton>
                </Grid>
                <Grid item justify="center" xs={8} style={{ paddingTop: '24px' }}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            style={{ width: '150px' }}
                            disableToolbar
                            variant="inline"
                            value={fromDate}
                            onChange={handleFromDateChange}
                            minDate={new Date()}
                            format="MM/dd/yyyy"
                            size="small"
                            InputProps={{ disableUnderline: true }}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                {[1, 2, 3, 4, 5, 6].map((e, index) => {
                    return (
                        <Grid item xs={4} key={index}>
                            <ViewCard onSetCheckInForm={props.onSetCheckInForm} />
                        </Grid>
                    )
                })}
            </Grid>
        </div>
    )
}

export default ViewByDay
