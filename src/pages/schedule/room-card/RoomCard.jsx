import { Card, CardContent, Divider, IconButton, Typography } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import CheckIcon from '@material-ui/icons/Check'
import RemoveIcon from '@material-ui/icons/Remove'
import React, { useState } from 'react'
import './room-card.scss'

function RoomCard(props) {
    const [isSelected, setSelected] = useState(false)

    return (
        <div className={"room-card-container " + (isSelected ? "border-red" : "")}>
            <Card>
                <CardContent style={{ padding: '0px' }}>
                    <div className="room-card-header display-flex">
                        <Typography
                            variant="subtitle1"
                            style={{ color: '#21A0A3', marginRight: '10px' }}
                        >
                            King room
                        </Typography>
                        <IconButton
                            style={{ outline: 'none' }}
                            size="small"
                            onClick={() => setSelected(!isSelected)}
                        >
                            {!isSelected ? <AddIcon /> : <RemoveIcon />}
                        </IconButton>
                    </div>
                    <Divider />
                    <div className="room-card-body display-flex">
                        <CheckIcon style={{ marginRight: '10px' }} />
                        <Typography variant="subtitle1" className="mr-auto">
                            Is clean
                        </Typography>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export default RoomCard
