import React from 'react'

/**================== Third Library =========================== */
import {
    Button,
    TextField,
    Typography,
    withStyles,
    InputAdornment,
    IconButton,
} from '@material-ui/core'
import { Visibility } from '@material-ui/icons'
import { useForm } from 'react-hook-form'
/** ========================================================== */

/** ======================= Component ==================================== */
import CustomContainer from '../../components/container/CustomContainer'
import RegisterImage from '../../assets/img/register.jpg'
import regisStyle from './registerpage.style'
/** ==================================================================== */

/** Regex */
import { EMAIL_REGEX as emailRegex, PASS_REGEX as passRegex } from '../../constants/Regex'

/** */

function RegisterPage(props) {
    const { classes } = props
    const { register, handleSubmit, errors } = useForm()

    const onSubmit = (data) => {
        console.log(data)
    }
    return (
        <CustomContainer LoginImage={RegisterImage}>
            <form onSubmit={handleSubmit(onSubmit)} className={classes.formContainer}>
                <Typography variant="h4" color="secondary">
                    Register
                </Typography>
                <TextField
                    id="outlined-basic"
                    label="Email"
                    color="secondary"
                    size="small"
                    name="Email"
                    fullWidth
                    inputRef={register({
                        required: 'Email is required',
                        pattern: {
                            value: emailRegex,
                            message: 'Must be an email: abc@abc.com',
                        },
                    })}
                />
                {errors.Email ? (
                    <Typography variant="caption" color="error" align="left">
                        {errors.Email.message}
                    </Typography>
                ) : (
                    ''
                )}
                <TextField
                    id="outlined-basic"
                    label="Password"
                    color="secondary"
                    size="small"
                    name="password"
                    type="password"
                    inputRef={register({
                        required: 'password is required',
                        pattern: {
                            value: passRegex,
                            message:
                                'Contain at least 8 characters || contain at least 1 number || contain at least 1 lowercase character (a-z) || contain at least 1 uppercase character (A-Z) || contains only 0-9a-zA-Z',
                        },
                    })}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    // onClick={handleClickShowPassword}
                                    // onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                    size="small"
                                    style={{ outline: 'none' }}
                                >
                                    {/* {values.showPassword ? <Visibility /> : <VisibilityOff />} */}
                                    <Visibility />
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                    fullWidth
                />
                {errors.password ? (
                    <Typography variant="caption" color="error" align="left">
                        {errors.password.message}
                    </Typography>
                ) : (
                    ''
                )}
                <TextField
                    id="outlined-basic"
                    label="Confirm Password"
                    color="secondary"
                    size="small"
                    fullWidth
                ></TextField>
                <Button
                    color="secondary"
                    style={{ outline: 'none' }}
                    variant="contained"
                    fullWidth
                    type="submit"
                >
                    Register
                </Button>
                <Typography variant="subtitle2" style={{ color: '#A7A7A7' }}>
                    or
                </Typography>
                <Button style={{ outline: 'none' }} href="#" color="primary">
                    Already have account?
                </Button>
            </form>
        </CustomContainer>
    )
}

export default withStyles(regisStyle, { withTheme: true })(RegisterPage)
