import React, { useState } from 'react'
import BaseTable from '../../../components/base-table/BaseTable'
import InventoryContainer from '../../../components/container/inventory_container/InventoryContainer'
import { addDays } from 'date-fns'
import Grid from '@material-ui/core/Grid'
import DateFnsUtils from '@date-io/date-fns'
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import { Button, IconButton, makeStyles, TextField } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { format } from 'date-fns/esm'
import { DatePicker } from '../../../components/date-picker/DatePicker'

// Colums for BaseTable Component
const columns = ['Date import', 'Total amount', 'Total price', 'Notes', 'Modify']

const row = [
    {
        dateImport: '10/8/2020',
        totalAmount: '2',
        totalPrice: '36$',
        notes: 'dit con me',
        modify: 'update',
    },
    {
        dateImport: '10/9/2020',
        totalAmount: '2',
        totalPrice: '35$',
        notes: 'dit con me',
        modify: 'update',
    },
    {
        dateImport: '10/10/2020',
        totalAmount: '2',
        totalPrice: '30$',
        notes: 'dit con me',
        modify: 'update',
    },
]

//

const useStyles = makeStyles(() => ({
    formHeader: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    formBody: {
        display: 'flex',
        justifyContent: 'space-evenly',
    },
}))

export default function InventoryImportPage() {
    const [fromDate, setFromdDate] = useState(new Date())
    const [toDate, setToDate] = useState(addDays(fromDate, 1))
    const classes = useStyles()
    const handleFromDateChange = (date) => {
        setFromdDate(date)
    }

    const handleToDateChange = (date) => {
        setToDate(date)
    }

    const onSearch = () => {
        const newFromDate = format(fromDate, 'MM/dd/yyyy')
        const newToDate = format(toDate, 'MM/dd/yyyy')
        console.log(`From date: ${newFromDate} || To Date: ${newToDate} `)
    }

    return (
        <InventoryContainer>
            <DatePicker />
            <BaseTable columns={columns} rows={row} />
            <form>
                <div className={classes.formHeader}>
                    <h5>Adding</h5>
                    <Button varient="outlined" color="primary">
                        Save
                    </Button>
                </div>
                <hr />
                <div className={classes.formBody}>
                    <TextField label="Find" style={{ marginRight: '20px' }} />
                    <TextField label="Notes" />
                </div>
            </form>
        </InventoryContainer>
    )
}
