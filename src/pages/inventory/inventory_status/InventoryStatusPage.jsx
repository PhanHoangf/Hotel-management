import React from 'react'

import { useForm } from 'react-hook-form'
import { TextField } from '@material-ui/core'

import BaseTable from '../../../components/base-table/BaseTable'
import InventoryContainer from '../../../components/container/inventory_container/InventoryContainer'

const columns = ['Menu', 'Amount']

const rows = [
    { menu: 'Furniture', amount: '10' },
    { menu: 'Furniture', amount: '10' },
    { menu: 'Furniture', amount: '10' },
    { menu: 'Furniture', amount: '10' },
]

export default function InventoryStatusPage() {
    const { handleSubmit } = useForm()
    const onSubmit = (data) => {
        console.log(data)
    }
    return (
        <InventoryContainer>
            <form onSubmit={handleSubmit(onSubmit)} style={{ width: '100%' }}>
                <TextField
                    id="outlined-basic"
                    label="Find"
                    color="primary"
                    size="small"
                    name="Find"
                    fullWidth
                />
            </form>
            <BaseTable columns={columns} rows={rows} />
        </InventoryContainer>
    )
}
