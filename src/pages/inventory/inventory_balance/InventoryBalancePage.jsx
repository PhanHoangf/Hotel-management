import React from 'react'
import BaseTable from '../../../components/base-table/BaseTable'
import InventoryContainer from '../../../components/container/inventory_container/InventoryContainer'
import { DatePicker } from '../../../components/date-picker/DatePicker'

const columns = ['Date', 'Total amount modify', 'Total price modify', 'Details']

const rows = [{ date: '10/8/2020', totalAmountM: '5', totalPriceM: '30$', details: 'ahihi' }]

export default function InventoryBalancePage() {
    return (
        <InventoryContainer>
            <DatePicker />
            <BaseTable columns={columns} rows={rows} />
        </InventoryContainer>
    )
}
