import { Fab, Icon } from '@material-ui/core'
import React from 'react'
import BaseTable from '../../../components/base-table/BaseTable'
import InventoryContainer from '../../../components/container/inventory_container/InventoryContainer'
import { DatePicker } from '../../../components/date-picker/DatePicker'
import VisibilityIcon from '@material-ui/icons/Visibility'

const columns = ['Time', 'Account', 'Details']

const rows = [
    {
        time: '07/10/2020 15:31',
        account: '17520519',
        action: (() => (
            <Fab size="small" style={{ outline: 'none' }} color="primary" aria-label="add">
                <VisibilityIcon />
            </Fab>
        ))(),
    },
    {
        time: '07/10/2020 15:31',
        account: '17520519',
        action: (() => (
            <Fab size="small" style={{ outline: 'none' }} color="primary" aria-label="add">
                <VisibilityIcon />
            </Fab>
        ))(),
    },
]

export default function InventoryCheckPage() {
    return (
        <InventoryContainer>
            <DatePicker />
            <BaseTable columns={columns} rows={rows} />
        </InventoryContainer>
    )
}
