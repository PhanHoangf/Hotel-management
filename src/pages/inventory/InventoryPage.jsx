import React from 'react'

// Third library
import { Grid, makeStyles } from '@material-ui/core'
//-------------
import InventoryItems from './inventory_items/InventoryItems'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing(2),
    },
    bgColor: {
        backgroundColor: 'transparent',
    },
}))

function InventoryPage() {
    const classes = useStyles()
    return (
        <Grid container className={classes.root} spacing={1} className={classes.bgColor}>
            <Grid item xs={12}>
                <Grid container justify="flex-start" spacing={2}>
                    <InventoryItems />
                </Grid>
            </Grid>
        </Grid>
    )
}

export default InventoryPage
