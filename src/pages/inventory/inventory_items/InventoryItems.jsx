import React from 'react'

import { Grid, makeStyles, Paper, Typography } from '@material-ui/core'
import Icon from '@material-ui/core/Icon'
//-------------
import { INVENTORY as inventoryItems } from '../../../constants/PageItems'
import { useHistory } from 'react-router'

const useStyles = makeStyles(() => ({
    paper: {
        alignItems: 'center',
        cursor: 'pointer',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        height: 140,
        width: 150,
    },
    icon: {
        fontSize: '42px',
    },
}))

export default function InventoryItems() {
    const history = useHistory()
    const classes = useStyles()

    const onNavigate = (url) => history.push('/main/inventory/' + url)

    return inventoryItems.map((item) => {
        return (
            <Grid key={item.id} item onClick={() => onNavigate(item.url)}>
                <Paper className={classes.paper}>
                    <Icon className={classes.icon} style={{ color: `${item.color}` }}>
                        {item.icon}
                    </Icon>
                    <Typography varient="h5">{item.title}</Typography>
                </Paper>
            </Grid>
        )
    })
}
