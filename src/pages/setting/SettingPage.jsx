import React from 'react'

// Third library
import { Backdrop, CircularProgress, Grid, makeStyles } from '@material-ui/core'
import SettingItems from './setting_items/SettingItems'
//-------------
// import InventoryItems from './inventory_items/InventoryItems'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    control: {
        padding: theme.spacing(2),
    },
    bgColor: {
        backgroundColor: 'transparent',
    },
}))

function SettingPage() {
    const classes = useStyles()
    return (
        <Grid container className={classes.root} spacing={1} className={classes.bgColor}>
            <Grid item xs={6}>
                <Grid container justify="flex-start" spacing={2}>
                    <SettingItems />
                </Grid>
            </Grid>
        </Grid>
    )
}

export default SettingPage
