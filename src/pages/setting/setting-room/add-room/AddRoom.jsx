import React, { useEffect, useState } from 'react'
import {
    Button,
    Divider,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
} from '@material-ui/core'
import SaveIcon from '@material-ui/icons/Save'
import AddIcon from '@material-ui/icons/Add'
import ClearIcon from '@material-ui/icons/Clear'
import { useForm, Controller } from 'react-hook-form'
import './add-room.scss'
import { NUMBER_REGEX } from '../../../../constants/Regex'
import { getPricesRequest } from '../../../../redux/action/price.action'
import { connect } from 'react-redux'
import { getRoomTypeRequest } from '../../../../redux/action/roomType.action'
import { mappedPriceSelector, mappedRoomTypeSelector } from './add-room.selector'
import { createRoomRequest, updateRoomRequest } from '../../../../redux/action/rooms.action'
import usePrevious from '../../../../components/use-previous/usePrevious'

function AddRoomController() {
    const [isVisible, setVisible] = useState(false)

    function onSetVisible(visible) {
        setVisible(visible)
    }

    return {
        isVisible,
        onSetVisible,
    }
}

function AddRoom(props) {
    const { register, handleSubmit, errors, getValues, control, setValue } = useForm()

    const controller = AddRoomController()

    const onSubmit = () => {
        const values = getValues()
        props.createRoomRequest(values)
        controller.onSetVisible(false)
        props.setLoading(true)
    }

    const onCloseForm = () => {
        controller.onSetVisible(false)
        props.close()
    }

    const onUpdateRoom = () => {
        const values = getValues()
        props.updateRoomRequest(props.entity._id, values)
        props.setLoading(true)
        onCloseForm()
    }

    const prevEntity = usePrevious(props.entity)

    useEffect(() => {
        props.getPricesRequest()
        props.getRoomTypeRequest()
    }, [])

    useEffect(() => {
        if (prevEntity !== props.entity) {
            setValue('nameRoom', props.entity.nameRoom ? props.entity.nameRoom : '')
            setValue('typeRoom', props.entity.typeRoom ? props.entity.typeRoom : '')
            setValue('roomPricing', props.entity.roomPricing ? props.entity.roomPricing : '')
            setValue('description', props.entity.description ? props.entity.description : '')
            setValue('priority', props.entity.priority ? props.entity.priority : '')
        }
    }, [{}])
    return (
        <form className="setting-add-room-container" onSubmit={handleSubmit(onSubmit)}>
            {!controller.isVisible && !props.isUpdate ? (
                <Button
                    color="primary"
                    variant="outlined"
                    startIcon={<AddIcon />}
                    style={{ outline: 'none', marginLeft: '10px' }}
                    onClick={() => controller.onSetVisible(!controller.isVisible)}
                >
                    Add
                </Button>
            ) : (
                <React.Fragment>
                    <div className="pl-10 pt-10 add-room-header">
                        <Typography variant="subtitle1" style={{ textTransform: 'uppercase' }}>
                            {props.isUpdate ? 'Update' : 'Add new'}
                        </Typography>
                        {props.isUpdate ? (
                            <Button
                                color="primary"
                                startIcon={<SaveIcon />}
                                style={{ outline: 'none' }}
                                className="ml-auto"
                                onClick={onUpdateRoom}
                            >
                                Save
                            </Button>
                        ) : (
                            <Button
                                color="primary"
                                startIcon={<AddIcon />}
                                style={{ outline: 'none' }}
                                className="ml-auto"
                                type="submit"
                            >
                                Add
                            </Button>
                        )}
                        <Button
                            color="secondary"
                            startIcon={<ClearIcon />}
                            style={{ outline: 'none' }}
                            onClick={onCloseForm}
                        >
                            Cancel
                        </Button>
                    </div>
                    <Divider />
                    <div className="mt-15 pl-10">
                        <Grid container justify="flex-start" spacing={3} alignItems="center">
                            <Grid item xs={12}>
                                <Controller
                                    as={<TextField label="Room name" fullWidth />}
                                    name="nameRoom"
                                    control={control}
                                    defaultValue={''}
                                    rules={{ required: true }}
                                />
                                {errors.roomName ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Room name is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl fullWidth>
                                    <InputLabel id="Roomtype">Room Type</InputLabel>
                                    <Controller
                                        control={control}
                                        name="typeRoom"
                                        rules={{ required: true }}
                                        defaultValue={''}
                                        as={
                                            <Select labelId="Roomtype" fullWidth>
                                                {props.roomType.map((entity) => {
                                                    return (
                                                        <MenuItem
                                                            key={entity._id}
                                                            value={`${entity._id}` || ''}
                                                        >
                                                            {entity.nameType}
                                                        </MenuItem>
                                                    )
                                                })}
                                            </Select>
                                        }
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <FormControl fullWidth>
                                    <InputLabel id="Price">Price</InputLabel>
                                    <Controller
                                        control={control}
                                        name="roomPricing"
                                        defaultValue={''}
                                        rules={{ required: true }}
                                        as={
                                            <Select labelId="Price" fullWidth>
                                                {props.price.map((entity) => {
                                                    return (
                                                        <MenuItem
                                                            key={entity._id}
                                                            value={`${entity._id}` || ''}
                                                        >
                                                            {entity.pricingName}
                                                        </MenuItem>
                                                    )
                                                })}
                                            </Select>
                                        }
                                    />
                                </FormControl>
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={<TextField label="Description" fullWidth />}
                                    name="description"
                                    control={control}
                                    defaultValue={''}
                                    rules={{ required: true }}
                                />
                                 {errors.description ? (
                                    <Typography variant="caption" color="error" align="left">
                                        this field is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={<TextField label="Priority" fullWidth />}
                                    name="priority"
                                    control={control}
                                    defaultValue={''}
                                    rules={
                                        ({ required: true },
                                        {
                                            pattern: {
                                                value: NUMBER_REGEX,
                                                message: 'number only',
                                            },
                                        })
                                    }
                                />
                                {errors.priority ? (
                                    <Typography variant="caption" color="error" align="left">
                                        {errors.priority.message}
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                        </Grid>
                    </div>
                </React.Fragment>
            )}
        </form>
    )
}

const mapStateToProps = (state) => {
    return {
        price: mappedPriceSelector(state.price),
        roomType: mappedRoomTypeSelector(state.roomType),
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getPricesRequest: () => {
            dispatch(getPricesRequest())
        },
        getRoomTypeRequest: () => {
            dispatch(getRoomTypeRequest())
        },
        createRoomRequest: (data) => {
            dispatch(createRoomRequest(data))
        },
        updateRoomRequest: (id, data) => {
            dispatch(updateRoomRequest(id, data))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddRoom)
