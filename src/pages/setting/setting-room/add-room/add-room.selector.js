import { createSelector } from 'reselect'

const priceSelector = (state) => state
const roomTypeSelector = (state) => state

export const mappedPriceSelector = createSelector(priceSelector, (prices) => {
    return prices.map((entity) => {
        return {
            _id: entity._id,
            pricingName: entity.pricingName,
        }
    })
})

export const mappedRoomTypeSelector = createSelector(roomTypeSelector, (roomTypes) => {
    return roomTypes.map((entity) => {
        return {
            _id: entity._id,
            nameType: entity.nameType,
        }
    })
})
