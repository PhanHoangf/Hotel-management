import { createSelector } from 'reselect'

const roomsSelector = (state) => state

export const mappedRoomsSelector = createSelector(roomsSelector, (rooms) => {
    return rooms.map((entity) => {
        return {
            _id: entity._id,
            nameRoom: entity.nameRoom,
            typeRoom: entity.typeRoom.nameType,
            description: entity.description,
            roomPricing: entity.roomPricing.pricingName,
            priority: entity.priority,
        }
    })
})


