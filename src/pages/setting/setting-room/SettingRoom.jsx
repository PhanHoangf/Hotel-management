import { Container, TextField } from '@material-ui/core'
import React from 'react'
import { connect } from 'react-redux'
import CustomBackdrop from '../../../components/back-drop/CustomBackdrop'
import BaseTable from '../../../components/base-table/BaseTable'
import ConfirmModal from '../../../components/confirm-modal/ConfirmModal'
import CustomSnackBar from '../../../components/snack-bar/CustomSnackBar'
import Spinner from '../../../components/spinner/Spinner'
import { getPricesRequest } from '../../../redux/action/price.action'
import { deleteRoomRequest, getRoomsRequest } from '../../../redux/action/rooms.action'
import { DeleteButton, ModifyButton } from '../setting-room-type/RoomType'
import AddRoom from './add-room/AddRoom'
import './setting-room.scss'
import { mappedRoomsSelector } from './setting-room.selector'

const columns = ['Room name', 'Room type', 'Description', 'Price', 'Priority', 'Actions']

class SettingRoom extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isLoading: true,
            openModal: false,
            deletedId: null,
            openSnackBar: false,
            isUpdate: false,
            isUpdated: false,
            updateEntity: {},
            openSnackBar: false,
            message: '',
            status: '',
        }
    }

    componentDidMount() {
        this.props.getRoomsRequest()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.room !== this.props.room) {
            this.setState({ isLoading: false })
            this.setSnackbar('success', true, {
                deletedId: this.state.deletedId,
                isUpdated: this.state.isUpdated,
            })
        }
    }

    _mappedRoom = (data) => {
        if (!data) return
        return data.map((entity) => {
            return {
                nameRoom: entity.nameRoom,
                typeRoom: entity.typeRoom,
                description: entity.description,
                roomPricing: entity.roomPricing,
                priority: entity.priority,
                actions: [
                    (() => (
                        <ModifyButton key={1} entity={entity} function={this.onOpenUpdateForm} />
                    ))(),
                    (() => (
                        <DeleteButton key={2} id={entity._id} function={this.onHandleOpenModal} />
                    ))(),
                ],
            }
        })
    }

    onHandleOpenModal = (deletedId) => {
        this.setState({
            openModal: !this.state.openModal,
            deletedId,
        })
    }

    onOpenUpdateForm = (entity) => {
        this.setState({
            isUpdate: true,
            isUpdated: true,
            updateEntity: entity,
        })
    }

    onCloseUpdateForm = () => {
        this.setState({
            isUpdate: false,
            updateEntity: {},
        })
    }

    onHanldeCloseModal = (deletedId) => {
        // console.log(deletedId)
        this.setState({
            openModal: false,
        })
        if (deletedId) {
            this.props.deleteRoomRequest(deletedId)
        }
    }

    setSnackbar(status, open, option) {
        if (option.deletedId) {
            this.setState({
                openSnackBar: open,
                message: 'Room has been deleted',
                status,
                deletedId: null,
            })
        }
        if (option.isUpdated) {
            this.setState({
                openSnackBar: open,
                message: 'Room has been updated',
                status,
                isUpdated: false,
            })
        } else return
    }

    setLoading = (value) => {
        this.setState({
            isLoading: value,
        })
    }

    onHandleCloseSnackbar = () => {
        this.setState({
            openSnackBar: false,
        })
    }

    render() {
        return (
            <Container maxWidth="xl" style={{ margin: '0', maxWidth: 'none' }}>
                <div className="setting-room-container">
                    <div className="left-container">
                        <TextField label="Search" fullWidth />
                        <BaseTable columns={columns} rows={this._mappedRoom(this.props.room)} />
                    </div>
                    <div className="divider"></div>
                    <div className="right-container">
                        <AddRoom
                            isUpdate={this.state.isUpdate}
                            close={this.onCloseUpdateForm}
                            entity={this.state.updateEntity}
                            setLoading={this.setLoading}
                        />
                    </div>
                    <ConfirmModal
                        state={this.state}
                        setLoading={this.setLoading}
                        onHanldeCloseModal={this.onHanldeCloseModal}
                    />
                    <CustomBackdrop open={this.state.isLoading}></CustomBackdrop>
                    <CustomSnackBar
                        open={this.state.openSnackBar}
                        message={this.state.message}
                        status={this.state.status}
                        onHandleCloseSnackbar={this.onHandleCloseSnackbar}
                    />
                </div>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        room: mappedRoomsSelector(state.room),
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRoomsRequest: () => {
            dispatch(getRoomsRequest())
        },
        deleteRoomRequest: (id) => {
            dispatch(deleteRoomRequest(id))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingRoom)
