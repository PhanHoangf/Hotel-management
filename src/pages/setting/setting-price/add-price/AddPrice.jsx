import React, { useRef, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import SaveIcon from '@material-ui/icons/Save'
import AddIcon from '@material-ui/icons/Add'
import ClearIcon from '@material-ui/icons/Clear'
import { Button, Divider, Grid, InputAdornment, TextField, Typography } from '@material-ui/core'
import NumberFormat from 'react-number-format'
import DateFnsUtils from '@date-io/date-fns'
import addHours from 'date-fns/addHours'
import { KeyboardTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import { createPriceRequest, updatePriceRequest } from '../../../../redux/action/price.action'
import { useEffect } from 'react'
import usePrevious from '../../../../components/use-previous/usePrevious'
import { connect } from 'react-redux'

function AddPriceController() {
    const [isVisible, setVisible] = useState(false)

    function onSetVisible(visible) {
        setVisible(visible)
    }

    function stringToNumber(str) {
        if (typeof str === 'number') return str
        if (str.includes(',')) {
            return +str.replace(/,/g, '')
        }
    }

    function formatData(data = {}) {
        console.log(data)
        if (!data) return
        return {
            pricingName: data.namePrice,
            firstBlock: data.firstHour,
            pricingFirstBlock: stringToNumber(data.firstHourPrice),
            pricingNextBlock: stringToNumber(data.nextHour),
            pricingOvernight: stringToNumber(data.overnightPrice),
            pricingDay: stringToNumber(data.dayPrice),
            timeReceiveRoom: data.checkIn,
            timeCheckoutRoom: data.checkOut,
            pricingOther: stringToNumber(data.extraCharge),
            firstHour: +data.firstHour,
            holidayPricing: {
                dayPrice: stringToNumber(data.weekdayDayPrice),
                firstHour: stringToNumber(data.weekdayFirstHour),
                nextHour: stringToNumber(data.weekdayNextHour),
                overnightPrice: stringToNumber(data.weekdayOvernightPrice),
            },
        }
    }

    return {
        isVisible,
        onSetVisible,
        formatData,
    }
}

function AddPrice(props) {
    const { pricingName } = props.entity
    const { handleSubmit, errors, getValues, control, setValue, reset } = useForm()

    const prevEntity = usePrevious(props.entity)

    const [checkInTime, setCheckInTime] = React.useState(new Date())
    const [checkOutTime, setCheckOutTime] = React.useState(addHours(new Date(), 2))

    const onSubmit = () => {
        const formatedValues = controller.formatData(getValues())
        props.createPriceRequest(formatedValues)
        props.setLoading(true)
        onCloseForm()
    }

    const onCloseForm = () => {
        controller.onSetVisible(false)
        props.close()
    }

    const onUpdateRoom = () => {
        const id = props.entity._id
        const values = controller.formatData(getValues())
        props.updatePriceRequest(id, values)
        props.setLoading(true)
        onCloseForm()
    }

    useEffect(() => {
        if (prevEntity !== props.entity) {
            const { dayPrice, firstHour, nextHour, overnightPrice } = props.entity.holidayPricing
                ? props.entity.holidayPricing
                : {}
            setValue('namePrice', props.entity.pricingName)
            setValue('firstHour', props.entity.firstBlock)
            setValue('firstHourPrice', props.entity.pricingFirstBlock)
            setValue('nextHour', props.entity.pricingNextBlock)
            setValue('checkIn', props.entity.timeReceiveRoom)
            setValue('checkOut', props.entity.timeCheckoutRoom)
            setValue('overnightPrice', props.entity.pricingOvernight)
            setValue('dayPrice', props.entity.pricingDay)
            setValue('extraCharge', props.entity.pricingOther)
            setValue('weekdayDayPrice', dayPrice ? dayPrice : '')
            setValue('weekdayFirstHour', firstHour ? firstHour : '')
            setValue('weekdayNextHour', nextHour ? nextHour : '')
            setValue('weekdayOvernightPrice', overnightPrice ? overnightPrice : '')
        }
    }, [{}])

    const controller = AddPriceController()

    return (
        <form className="setting-add-room-container" onSubmit={handleSubmit(onSubmit)}>
            {!controller.isVisible && !props.isUpdate ? (
                <Button
                    color="primary"
                    variant="outlined"
                    startIcon={<AddIcon />}
                    style={{ outline: 'none', marginLeft: '10px' }}
                    onClick={() => controller.onSetVisible(!controller.isVisible)}
                >
                    Add
                </Button>
            ) : (
                <React.Fragment>
                    <div className="pl-10 pt-10 add-room-header">
                        <Typography variant="subtitle1" style={{ textTransform: 'uppercase' }}>
                            {props.isUpdate ? 'Update' : 'Add new'}
                        </Typography>
                        {props.isUpdate ? (
                            <Button
                                color="primary"
                                startIcon={<SaveIcon />}
                                style={{ outline: 'none' }}
                                className="ml-auto"
                                onClick={onUpdateRoom}
                            >
                                Save
                            </Button>
                        ) : (
                            <Button
                                color="primary"
                                startIcon={<AddIcon />}
                                style={{ outline: 'none' }}
                                className="ml-auto"
                                type="submit"
                            >
                                Add
                            </Button>
                        )}
                        <Button
                            color="secondary"
                            startIcon={<ClearIcon />}
                            style={{ outline: 'none' }}
                            onClick={onCloseForm}
                        >
                            Cancel
                        </Button>
                    </div>
                    <Divider />
                    <div className="mt-15 pl-10">
                        <Grid container justify="flex-start" spacing={3} alignItems="center">
                            <Grid item xs={12}>
                                <Controller
                                    as={
                                        <TextField
                                            label="Name price"
                                            // inputRef={register({ required: true })}
                                            // defaultValue={props.entity ? props.entity.nameRoom : ''}
                                            fullWidth
                                        />
                                    }
                                    name="namePrice"
                                    control={control}
                                    defaultValue={''}
                                    rules={{ required: true }}
                                />

                                {errors.namePrice ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Price name is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={
                                        <TextField
                                            label="First hours"
                                            id="standard-start-adornment"
                                            // inputRef={register({ required: true })}
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        Hours
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="firstHour"
                                    control={control}
                                    defaultValue=""
                                    rules={{ required: true }}
                                />

                                {errors.firstHour ? (
                                    <Typography variant="caption" color="error" align="left">
                                        First hour is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={6}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="First hour price"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="firstHourPrice"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.firstHourPrice ? (
                                    <Typography variant="caption" color="error" align="left">
                                        First hour is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={6}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="Next hour price"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="nextHour"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.nextHour ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Next hour is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={6}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <Controller
                                        as={
                                            <KeyboardTimePicker
                                                margin="normal"
                                                label="Check-in time"
                                                KeyboardButtonProps={{
                                                    'aria-label': 'change time',
                                                }}
                                            />
                                        }
                                        defaultValue={checkInTime}
                                        name="checkIn"
                                        control={control}
                                        rules={{ required: true }}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={6}>
                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <Controller
                                        as={
                                            <KeyboardTimePicker
                                                margin="normal"
                                                label="Check-out time"
                                                KeyboardButtonProps={{
                                                    'aria-label': 'change time',
                                                }}
                                            />
                                        }
                                        name="checkOut"
                                        control={control}
                                        rules={{ required: true }}
                                        defaultValue={checkOutTime}
                                    />
                                </MuiPickersUtilsProvider>
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="Overnight price"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="overnightPrice"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.overnightPrice ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Overnight price is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="Day price"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="dayPrice"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.dayPrice ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Day price is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="Extra charge"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="extraCharge"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.extraCharge ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Extra charge is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Typography
                                    variant="h6"
                                    color="primary"
                                    style={{ padding: '0', margin: '0' }}
                                >
                                    Weekends and Holidays
                                </Typography>
                                <Typography variant="subtitle2" color="primary">
                                    (Options*)
                                </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="First hour"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="weekdayFirstHour"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.weekdayFirstHour ? (
                                    <Typography variant="caption" color="error" align="left">
                                        First hour is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={6}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="Next hour"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="weekdayNextHour"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.weekdayNextHour ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Next hour is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="Day price"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="weekdayDayPrice"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.weekdayNextHour ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Day price is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={
                                        <NumberFormat
                                            customInput={TextField}
                                            thousandSeparator={true}
                                            label="Overnight price"
                                            fullWidth
                                            InputProps={{
                                                endAdornment: (
                                                    <InputAdornment position="end">
                                                        &#8363;
                                                    </InputAdornment>
                                                ),
                                            }}
                                        />
                                    }
                                    name="weekdayOvernightPrice"
                                    control={control}
                                    rules={{ required: true }}
                                    defaultValue=""
                                />
                                {errors.weekdayOvernightPrice ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Weekday Overnight Price is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                        </Grid>
                    </div>
                </React.Fragment>
            )}
        </form>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        createPriceRequest: (data) => {
            dispatch(createPriceRequest(data))
        },
        updatePriceRequest: (data, id) => {
            dispatch(updatePriceRequest(data, id))
        },
    }
}

export default connect(null, mapDispatchToProps)(AddPrice)
