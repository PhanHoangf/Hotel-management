import { Container, Divider, makeStyles, TextField, withStyles } from '@material-ui/core'
import React from 'react'
import { connect } from 'react-redux'
import BaseTable from '../../../components/base-table/BaseTable'
import { deletePriceRequest, getPricesRequest } from '../../../redux/action/price.action'
import AddPrice from './add-price/AddPrice'
import './setting-price.scss'
import { DeleteButton, ModifyButton } from '../setting-room-type/RoomType'
import CustomBackdrop from '../../../components/back-drop/CustomBackdrop'
import ConfirmModal from '../../../components/confirm-modal/ConfirmModal'
import CustomSnackBar from '../../../components/snack-bar/CustomSnackBar'

class SettingPriceController extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isUpdate: false,
            isLoading: true,
            updateEntity: {},
            deletedId: null,
            openModal: false,
            openSnackBar: false,
            messageSnackBar: '',
            statusSnackBar: '',
            isUpdated: false,
        }
        this.onOpenUpdateForm = this.onOpenUpdateForm.bind(this)
        this.onHandleOpenModal = this.onHandleOpenModal.bind(this)
        this.onHanldeCloseModal = this.onHanldeCloseModal.bind(this)
        this.onCloseUpdateForm = this.onCloseUpdateForm.bind(this)
        this.setLoading = this.setLoading.bind(this)
        this.onHandleCloseSnackbar = this.onHandleCloseSnackbar.bind(this)
        this.setSnackbar = this.setSnackbar.bind(this)
    }

    componentDidMount() {
        this.props.getPricesRequest()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.prices !== this.props.prices) {
            this.setLoading(false)
            this.setSnackbar('success', true, {
                deletedId: this.state.deletedId,
                isUpdated: this.state.isUpdated,
            })
        }
    }

    mappedPrices(prices) {
        if (!prices) return
        return prices.map((price) => {
            return {
                price: price.pricingName,
                firstHour: price.pricingFirstBlock,
                nextHour: price.pricingNextBlock,
                overnight: price.pricingOvernight,
                dayPrice: price.pricingDay,
                extraCharge: price.pricingOther,
                modify: [
                    (() => (
                        <ModifyButton key={1} entity={price} function={this.onOpenUpdateForm} />
                    ))(),
                    (() => (
                        <DeleteButton key={2} id={price._id} function={this.onHandleOpenModal} />
                    ))(),
                ],
            }
        })
    }

    get rows() {
        return this.mappedPrices(this.props.prices)
    }

    get columns() {
        return [
            'Price',
            'First hours',
            'Next hours',
            'Overnight',
            'Day price',
            'Extra charging',
            'Modify',
        ]
    }

    onOpenUpdateForm(entity) {
        this.setState({
            updateEntity: entity,
            isUpdate: true,
            isUpdated: true
        })
    }

    onHandleOpenModal(id) {
        this.setState({
            openModal: true,
            deletedId: id,
        })
    }

    onHanldeCloseModal(id) {
        this.setState({
            deletedId: id,
            openModal: false,
        })
        if (id) {
            this.props.deletePriceRequest(id)
        }
    }

    onCloseUpdateForm() {
        this.setState({
            updateEntity: {},
            isUpdate: false,
        })
    }

    setLoading(value) {
        this.setState({
            isLoading: value,
        })
    }

    setSnackbar(status, open, option) {
        console.log(option)
        if (option.deletedId) {
            this.setState({
                openSnackBar: open,
                messageSnackBar: 'Price has been deleted',
                statusSnackBar: status,
                deletedId: null,
            })
        } else if (option.isUpdated) {
            this.setState({
                openSnackBar: open,
                messageSnackBar: 'Price has been updated',
                statusSnackBar: status,
                isUpdated: false,
            })
        } else return
    }

    onHandleCloseSnackbar() {
        this.setState({
            openSnackBar: false,
        })
    }
    render() {
        return
    }
}

class SettingPrice extends SettingPriceController {
    render() {
        return (
            <Container maxWidth="xl" style={{ margin: '0', maxWidth: 'none' }}>
                <div className="setting-price-container">
                    <div className="left-container">
                        <TextField label="Search" fullWidth></TextField>
                        <BaseTable columns={this.columns} rows={this.rows} />
                    </div>
                    <div className="divider"></div>
                    <div className="right-container">
                        <AddPrice
                            isUpdate={this.state.isUpdate}
                            entity={this.state.updateEntity}
                            close={this.onCloseUpdateForm}
                            setLoading={this.setLoading}
                        />
                    </div>
                    <ConfirmModal
                        state={this.state}
                        onHanldeCloseModal={this.onHanldeCloseModal}
                        setLoading={this.setLoading}
                    />
                    <CustomSnackBar
                        open={this.state.openSnackBar}
                        message={this.state.messageSnackBar}
                        status={this.state.statusSnackBar}
                        onHandleCloseSnackbar={this.onHandleCloseSnackbar}
                    ></CustomSnackBar>
                    <CustomBackdrop open={this.state.isLoading}></CustomBackdrop>
                </div>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        prices: state.price,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getPricesRequest: () => {
            dispatch(getPricesRequest())
        },
        deletePriceRequest: (id) => {
            dispatch(deletePriceRequest(id))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingPrice, SettingPriceController)
