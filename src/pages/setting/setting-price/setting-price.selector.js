import { createSelector } from 'reselect'

const priceSelector = (state) => state.price

const mappedPriceSelector = createSelector(priceSelector, (prices) => prices.map((price) => price))
