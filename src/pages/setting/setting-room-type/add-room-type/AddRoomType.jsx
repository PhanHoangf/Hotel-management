import React, { useEffect, useState } from 'react'
import { Button, Divider, Grid, TextField, Typography } from '@material-ui/core'
import SaveIcon from '@material-ui/icons/Save'
import AddIcon from '@material-ui/icons/Add'
import ClearIcon from '@material-ui/icons/Clear'
import './addroomtype.scss'
import { Controller, useForm } from 'react-hook-form'
import { NUMBER_REGEX } from '../../../../constants/Regex'
import { connect } from 'react-redux'
import {
    createRoomTypeRequest,
    updateRoomTypeRequest,
} from '../../../../redux/action/roomType.action'
import usePrevious from '../../../../components/use-previous/usePrevious'

function AddRoomTypeController() {
    const [isVisible, setVisible] = useState(false)

    function onSetVisible(visible) {
        setVisible(visible)
    }

    function createRoomType(cb, data) {
        cb(data)
    }

    function resetForm(data) {
        data = {}
    }

    return {
        isVisible,
        onSetVisible,
        createRoomType,
        resetForm,
    }
}

function AddRoomType(props) {
    const { register, handleSubmit, errors, setValue, getValues, control } = useForm()
    const controller = AddRoomTypeController()

    const onSubmit = (data) => {
        controller.createRoomType(props.createRoomTypeRequest, data)
        controller.onSetVisible(!controller.isVisible)
        props.setLoading(true)
        onCloseModal()
    }

    const prevRoomType = usePrevious(props.entity)

    const onUpdateRoomType = () => {
        const value = getValues()
        props.updateRoomTypeRequest(props.entity._id, value)
        props.setLoading(true)
        onCloseModal()
    }

    const onCloseModal = () => {
        controller.onSetVisible(false)
        props.close()
    }

    useEffect(() => {
        if (prevRoomType !== props.entity) {
            setValue('nameType', props.entity.nameType ? props.entity.nameType : '')
            setValue('description', props.entity.description ? props.entity.description : '')
            setValue('priority', props.entity.priority ? props.entity.priority : '')
        }
    }, [{}])
    return (
        <form className="add-room-type-container" onSubmit={handleSubmit(onSubmit)}>
            {!controller.isVisible && !props.open ? (
                <Button
                    color="primary"
                    variant="outlined"
                    startIcon={<AddIcon />}
                    style={{ outline: 'none', marginLeft: '10px' }}
                    onClick={() => controller.onSetVisible(!controller.isVisible)}
                >
                    Add
                </Button>
            ) : (
                <React.Fragment>
                    <div className="pl-10 pt-10 add-room-type-header">
                        <Typography variant="subtitle1" style={{ textTransform: 'uppercase' }}>
                            Add new
                        </Typography>
                        {props.open ? (
                            <Button
                                color="primary"
                                startIcon={<SaveIcon />}
                                style={{ outline: 'none' }}
                                className="ml-auto"
                                onClick={onUpdateRoomType}
                            >
                                Save
                            </Button>
                        ) : (
                            <Button
                                color="primary"
                                startIcon={<AddIcon />}
                                style={{ outline: 'none' }}
                                className="ml-auto"
                                type="submit"
                            >
                                Add
                            </Button>
                        )}
                        <Button
                            color="secondary"
                            startIcon={<ClearIcon />}
                            style={{ outline: 'none' }}
                            onClick={onCloseModal}
                        >
                            Cancel
                        </Button>
                    </div>
                    <Divider />
                    <div className="mt-15 pl-10">
                        <Grid container justify="flex-start" spacing={3} alignItems="center">
                            <Grid item xs={12}>
                                <Controller
                                    as={<TextField label="Room type name" fullWidth />}
                                    name="nameType"
                                    control={control}
                                    defaultValue={''}
                                    rules={{ required: true }}
                                />

                                {errors.nameType ? (
                                    <Typography variant="caption" color="error" align="left">
                                        Room type name is required
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={<TextField label="Description" fullWidth />}
                                    name="description"
                                    control={control}
                                    defaultValue={''}
                                    rules={{ required: true }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Controller
                                    as={<TextField label="Priority" fullWidth />}
                                    name="priority"
                                    control={control}
                                    defaultValue={''}
                                    rules={
                                        ({ required: true },
                                        {
                                            pattern: {
                                                value: NUMBER_REGEX,
                                                message: 'number only',
                                            },
                                        })
                                    }
                                />

                                {errors.priority ? (
                                    <Typography variant="caption" color="error" align="left">
                                        {errors.priority.message}
                                    </Typography>
                                ) : (
                                    ''
                                )}
                            </Grid>
                        </Grid>
                    </div>
                </React.Fragment>
            )}
        </form>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        createRoomTypeRequest: (roomType) => {
            dispatch(createRoomTypeRequest(roomType))
        },
        updateRoomTypeRequest: (id, roomType) => {
            dispatch(updateRoomTypeRequest(id, roomType))
        },
    }
}

export default connect(null, mapDispatchToProps)(AddRoomType)
