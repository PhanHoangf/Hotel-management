import { Button, Container, TextField, Tooltip } from '@material-ui/core'
import React from 'react'
import { connect } from 'react-redux'
import BaseTable from '../../../components/base-table/BaseTable'
import { deleteRoomTypeRequest, getRoomTypeRequest } from '../../../redux/action/roomType.action'
import AddRoomType from './add-room-type/AddRoomType'
import './roomtype.scss'
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import ConfirmModal from '../../../components/confirm-modal/ConfirmModal'
import CustomSnackBar from '../../../components/snack-bar/CustomSnackBar'
import CustomBackdrop from '../../../components/back-drop/CustomBackdrop'

const columns = ['ID', 'Room type', 'Description', 'Priority', 'Actions']

// const rows = [{ roomType: 'Single room', description: 'single room', modify: '' }]

export function ModifyButton(props) {
    return (
        <Tooltip title="modify">
            <Button
                color="primary"
                startIcon={<EditIcon />}
                style={{ outline: 'none' }}
                type="submit"
                onClick={() => props.function(props.entity)}
            ></Button>
        </Tooltip>
    )
}

export function DeleteButton(props) {
    return (
        <Tooltip title="delete">
            <Button
                color="secondary"
                startIcon={<DeleteIcon />}
                style={{ outline: 'none' }}
                type="submit"
                onClick={() => props.function(props.id)}
            ></Button>
        </Tooltip>
    )
}

class RoomType extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            openModal: false,
            deletedId: null,
            openSnackbar: false,
            openUpdateForm: false,
            statusSnackbar: '',
            messageSnackbar: '',
            updateRoom: {},
            isLoading: true,
            isUpdated: false
        }
    }

    // !LIFE_CYCLE //
    componentDidMount() {
        this.props.getRoomTypeRequest()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.roomType !== this.props.roomType) {
            this.setState({
                isLoading: false,
            })
            this.setSnackbar('success', true, {
                deletedId: this.state.deletedId,
                isUpdated: this.state.isUpdated,
            })
        }
    }
    // !-----------------! //

    // !MAPPED_DATA //
    _mappedResult(data) {
        if (!data) return
        return data.map((entity) => {
            return {
                id: entity.index,
                roomType: entity.nameType,
                description: entity.description,
                priority: entity.priority,
                actions: [
                    (() => (
                        <ModifyButton key={0} entity={entity} function={this.onOpenUpdateForm} />
                    ))(),
                    (() => (
                        <DeleteButton key={1} id={entity._id} function={this.onHandleOpenModal} />
                    ))(),
                ],
            }
        })
    }
    // !-----------------! //

    // !ACTION //
    onHandleOpenModal = (deletedId) => {
        this.setState({
            openModal: !this.state.openModal,
            deletedId,
        })
    }

    onHanldeCloseModal = (deletedId) => {
        this.setState({
            openModal: !this.state.openModal,
        })
        if (deletedId) {
            this.onDeleteRoomType(deletedId)
        }
    }

    onHandleCloseSnackbar = () => {
        this.setState({
            openSnackbar: !this.state.openSnackbar,
        })
    }

    onDeleteRoomType = (id) => {
        this.props.deleteRoomTypeRequest(id)
    }

    onOpenUpdateForm = (updateRoom) => {
        this.setState({
            openUpdateForm: true,
            isUpdated: true,
            updateRoom,
        })
    }

    onCloseUpdateForm = () => {
        this.setState({
            openUpdateForm: false,
            updateRoom: {},
        })
    }
    // !-----------------! //

    // !SET_STATE //
    // setSnackBarState(deletedId) {
    //     if (deletedId) {
    //         this.setState({
    //             openSnackbar: !this.state.openSnackbar,
    //             messageSnackbar: `Room type with ID: ${this.state.deletedId} has been deleted`,
    //             statusSnackbar: 'success',
    //             deletedId: null,
    //         })
    //     } else return
    // }

    setSnackbar = (status, open, option) => {
        if (option.deletedId) {
            this.setState({
                openSnackbar: open,
                messageSnackbar: `Room type with ID: ${this.state.deletedId} has been deleted`,
                statusSnackbar: status,
                deletedId: null,
            })
        } else if (option.isUpdated) {
            this.setState({
                openSnackbar: open,
                messageSnackbar: 'Room type has been updated',
                statusSnackbar: status,
                isUpdated: false,
            })
        } else return
    }

    setRoomTypeId(deletedId) {
        this.setState({
            deletedId,
        })
    }

    setLoading = (value) => {
        this.setState({
            isLoading: value,
        })
    }
    // !-----------------! //

    render() {
        console.log(this.state.openSnackbar)
        const rows = this._mappedResult(this.props.roomType)
        return (
            <Container maxWidth="xl" style={{ margin: '0', maxWidth: 'none' }}>
                <div className="room-type-container">
                    <div className="left-container">
                        <TextField label="Search" fullWidth></TextField>
                        <BaseTable columns={columns} rows={rows} />
                    </div>
                    <div className="divider"></div>
                    <div className="right-container">
                        <AddRoomType
                            open={this.state.openUpdateForm}
                            entity={this.state.updateRoom}
                            close={this.onCloseUpdateForm}
                            setLoading={this.setLoading}
                        />
                    </div>
                    <CustomSnackBar
                        open={this.state.openSnackbar}
                        message={this.state.messageSnackbar}
                        status={'success'}
                        onHandleCloseSnackbar={this.onHandleCloseSnackbar}
                    ></CustomSnackBar>
                </div>
                <ConfirmModal
                    state={this.state}
                    onHanldeCloseModal={this.onHanldeCloseModal}
                    setLoading={this.setLoading}
                />

                <CustomBackdrop open={this.state.isLoading}></CustomBackdrop>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        roomType: state.roomType,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getRoomTypeRequest: () => {
            dispatch(getRoomTypeRequest())
        },
        deleteRoomTypeRequest: (id) => {
            dispatch(deleteRoomTypeRequest(id))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomType)
