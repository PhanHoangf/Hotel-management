import { Container, withStyles } from '@material-ui/core'
import React from 'react'
import notfoundStyle from './notfoundStyle'

function NotfoundPage(props) {
    const { classes } = props
    return <div className={classes.container}></div>
}

export default withStyles(notfoundStyle)(NotfoundPage)
