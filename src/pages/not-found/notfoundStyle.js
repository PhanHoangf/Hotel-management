import NotFoundImage from '../../assets/img/errorpage.jpg'

const styles = {
    container: {
        backgroundImage: `url(${NotFoundImage})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        height: '100vh',
    },
}

export default styles
