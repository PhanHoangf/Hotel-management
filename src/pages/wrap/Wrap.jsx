import React, { useState } from 'react'
import { Switch, Route, useRouteMatch } from 'react-router-dom'

import { Container, makeStyles } from '@material-ui/core'
import clsx from 'clsx'

import NavBar from '../../components/navbar/NavBar'
import InventoryPage from '../inventory/InventoryPage'
import InventoryStatusPage from '../inventory/inventory_status/InventoryStatusPage'
import InventoryImportPage from '../inventory/inventory_import/InventoryImportPage'
import HomePage from '../home/HomePage'
import InventoryBalancePage from '../inventory/inventory_balance/InventoryBalancePage'
import InventoryCheckPage from '../inventory/inventory_check/InventoryCheckPage'
import SettingPage from '../setting/SettingPage'
import SchedulePage from '../schedule/SchedulePage'
import RoomType from '../setting/setting-room-type/RoomType'
import SettingRoom from '../setting/setting-room/SettingRoom'
import CheckInPage from '../check-in/CheckInPage'
import SettingPrice from '../setting/setting-price/SettingPrice'
import PrivateRoute from '../../components/auth/PrivateRoute'

const useStyles = makeStyles((theme) => ({
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -20,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 220,
    },
}))

export default function Wrap() {
    const [isOpen, setSideNav] = useState(false)
    let { path } = useRouteMatch()
    const checkSideNav = (open) => {
        setSideNav(open)
    }
    const classes = useStyles()
    return (
        <Container maxWidth="xl" style={{ paddingRight: '0px' }}>
            <NavBar isOpen={isOpen} checkSideNav={checkSideNav} />
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: isOpen,
                })}
            >
                <div className={classes.drawerHeader}></div>
                <Switch>
                    <PrivateRoute path={`${path}/home`}>
                        <HomePage />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/inventory/add`}>
                        <InventoryImportPage />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/inventory/status`}>
                        <InventoryStatusPage />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/inventory/update`}>
                        <InventoryBalancePage />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/inventory/check`}>
                        <InventoryCheckPage />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/setting/rule`}>
                        <SettingPrice />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/setting/roomtype`}>
                        <RoomType />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/setting/room`}>
                        <SettingRoom />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/setting/menu`}>
                        <div>tui la menu ne</div>
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/setting/usersub`}>
                        <div>tui la usersub ne</div>
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/setting/report`}>
                        <div>tui la report ne</div>
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/setting/history`}>
                        <div>tui la history ne</div>
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/setting/othersetting`}>
                        <div>tui la othersetting ne</div>
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/inventory`}>
                        <InventoryPage />
                    </PrivateRoute>
                    <PrivateRoute exact path={`${path}/setting`}>
                        <SettingPage />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/schedule`}>
                        <SchedulePage />
                    </PrivateRoute>
                    <PrivateRoute path={`${path}/check-in`}>
                        <CheckInPage />
                    </PrivateRoute>
                </Switch>
            </main>
        </Container>
    )
}
