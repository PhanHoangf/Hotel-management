import React, { useState } from 'react'
import { connect } from 'react-redux'
import { signInRequest } from '../../redux/action/auth.action'
import { compose } from 'redux'
/** Third Library */
import { useForm } from 'react-hook-form'
import {
    Button,
    TextField,
    withStyles,
    Typography,
    InputAdornment,
    IconButton,
} from '@material-ui/core'
import { Visibility } from '@material-ui/icons'
/** */

/** Component */
import style from './loginpage.style'
import CustomContainer from '../../components/container/CustomContainer'
import LoginImage from '../../assets/img/login.jpg'
/** */

/** Regex */
import { EMAIL_REGEX as emailRegex } from '../../constants/Regex'
import { useHistory, useLocation } from 'react-router'
import CustomBackdrop from '../../components/back-drop/CustomBackdrop'
/** */

function LoginPage(props) {
    const { register, handleSubmit, errors } = useForm()
    const [isLoading, setLoading] = useState(false)
    let history = useHistory()
    let location = useLocation()
    const { classes } = props

    let from = location.state || { from: { pathname: '/main' } }

    const onSubmit = (data) => {
        setLoading(true)
        props.signInRequest(data, () => {
            setLoading(false)
            history.push('/main')
        })
    }

    return (
        <CustomContainer LoginImage={LoginImage}>
            <form onSubmit={handleSubmit(onSubmit)} className={classes.formContainer}>
                <Typography variant="h4" color="primary">
                    Login
                </Typography>
                <TextField
                    id="outlined-basic"
                    label="Username*"
                    variant="outlined"
                    fullWidth
                    name="userName"
                    inputRef={register({
                        required: 'Username is required',
                        pattern: {
                            value: emailRegex,
                            message: 'Username must be an email: abc@abc.com',
                        },
                    })}
                />
                {errors.userName ? (
                    <Typography variant="caption" color="error" align="left">
                        {errors.userName.message}
                    </Typography>
                ) : (
                    ''
                )}
                <TextField
                    id="outlined-basic"
                    label="Password*"
                    variant="outlined"
                    name="password"
                    type="password"
                    fullWidth
                    // required
                    inputRef={register({ required: true })}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    // onClick={handleClickShowPassword}
                                    // onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                    size="small"
                                    style={{ outline: 'none' }}
                                >
                                    {/* {values.showPassword ? <Visibility /> : <VisibilityOff />} */}
                                    <Visibility />
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                {errors.password ? (
                    <Typography variant="caption" color="error" align="left">
                        Password is required
                    </Typography>
                ) : (
                    ''
                )}
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    style={{ outline: 'none' }}
                    fullWidth
                >
                    Login
                </Button>
                <Typography variant="caption" style={{ color: '#A7A7A7' }}>
                    or
                </Typography>
                <Button color="secondary" style={{ outline: 'none' }} variant="contained" fullWidth>
                    Register
                </Button>
                <Button style={{ outline: 'none' }} href="#" color="primary">
                    Forgot Password?
                </Button>
            </form>
            <CustomBackdrop open={isLoading}></CustomBackdrop>
        </CustomContainer>
    )
}

const mapStateToProps = (state) => {
    return {
        isAuthenticate: state.auth.isAuthenticate,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signInRequest: (user, cb) => {
            dispatch(signInRequest(user, cb))
        },
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withStyles(style, { withTheme: true })
)(LoginPage)
