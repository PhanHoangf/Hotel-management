const styles = (theme) => ({
    formContainer: {
        textAlign: 'center',
        boxShadow: '2px 11px 22px 0px rgba(46,74,117,1)',
        borderRadius: '10px',
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        padding: '15px 30px 30px 30px',
        width: '35%',
        height: '90%',
    },
})

export default styles
