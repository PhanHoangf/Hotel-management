import React from 'react'
import {
    Grid,
    Icon,
    Button,
    Card,
    CardActions,
    CardContent,
    Typography,
    Box,
    Menu,
    MenuItem,
} from '@material-ui/core'
import { HomePageConst, MenuConst } from '../../constants/HomePage.const'
import { makeStyles } from '@material-ui/core/styles'
import LineChart from '../../components/chart/LineChart'

const useStyles = makeStyles(() => ({
    root: {
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: '20px 20px 0px 30px',
        flexWrap: 'wrap',
        minWidth: '323px',
    },
    button: {
        outline: 'none',
    },
    rotated: {
        transform: 'translateX(250%) rotate(-90deg)',
        display: 'inline-block',
        width: '10%',
    },
    menuItems: {
        alignItems: 'center',
        margin: '10px',
    },
}))

export default function HomePage() {
    const classes = useStyles()
    const [anchorEl, setAnchorEl] = React.useState(null)
    const items = HomePageConst.map((item, index) => {
        return (
            <Grid xs={3} item key={index}>
                <Card>
                    <Grid item className={classes.root}>
                        <Icon style={{ color: `${item.color}`, fontSize: '50px' }}>
                            {item.icon}
                        </Icon>
                        <CardContent>
                            <Typography component="h5" variant="h5">
                                {item.title}
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary">
                                2 {item.unit}
                            </Typography>
                        </CardContent>
                    </Grid>
                    <hr />
                    <CardActions style={{ outline: 'none' }}>
                        <Button className={classes.button}>
                            <Icon>{item.subIcon}</Icon>
                            <span style={{ marginLeft: '10px' }}>{item.subtitle}</span>
                        </Button>
                    </CardActions>
                </Card>
            </Grid>
        )
    })
    const menuItems = MenuConst.map((item, index) => {
        return (
            <MenuItem key={index} className={classes.menuItems}>
                <Icon style={{ marginRight: '15px', transform: 'translateY(-5px)' }}>
                    {item.icon}
                </Icon>

                <label>{item.title}</label>
            </MenuItem>
        )
    })
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }
    return (
        <Grid container spacing={3}>
            {items}
            <Grid
                item
                style={{ boxShadow: '11px 13px 5px -10px rgba(0,0,0,0.75)', width: '100%' }}
                xs={8}
            >
                <Box display="flex" flexDirection="row">
                    <span className={classes.rotated}>Add rooms</span>
                    <LineChart />
                </Box>

                <div style={{ textAlign: 'center' }}>Date</div>
            </Grid>
            <Grid item style={{ boxShadow: '11px 13px 5px -10px rgba(0,0,0,0.75)' }} xs={4}>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }}
                >
                    <h5 style={{ transform: 'translateY(10px)', color: 'blue' }}>History system</h5>
                    <Button aria-controls="menu" aria-haspopup="true" onClick={handleClick}>
                        <Icon style={{ fontSize: '32px' }}>list</Icon>
                    </Button>
                    <Menu
                        id="menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        {menuItems}
                    </Menu>
                </div>
                <hr />
            </Grid>
        </Grid>
    )
}
