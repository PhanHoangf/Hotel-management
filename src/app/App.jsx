import React from 'react'
import './App.scss'

/** ===================== Third library */
import { BrowserRouter as Router, Switch, Redirect } from 'react-router-dom'
/** ================================== */

/** ====================== Component  */
import Routes from '../routes/routes'
/** ================================ */

export default function App() {
    return (
        <Router>
            <Switch>
                {Routes.routesConfig(Routes.routes)}
                {/* <Redirect to="/404" /> */}
                <Redirect to="/main" />
            </Switch>
        </Router>
    )
}
