export const getDate = (date) => {
    const [day, month, year] = date.split('/')
    return {
        day: +day,
        month: +month - 1,
        year: +year,
    }
}
