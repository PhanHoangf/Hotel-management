import React from 'react'
import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
    withStyles,
    CssBaseline,
    Button,
} from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import styles from './navbar.style.js'
import clsx from 'clsx'
import { Link } from 'react-router-dom'

/**
 *  Import css
 */
import './navbar.scss'
import SideNav from '../sidenav/SideNav.jsx'
import InventoryPage from '../../pages/inventory/InventoryPage.jsx'

function NavBar(props) {
    const { classes, checkSideNav, isOpen } = props

    const toggleOpen = () => {
        props.checkSideNav(!isOpen)
    }

    return (
        <div style={{ display: 'flex', flexGrow: '1' }}>
            <CssBaseline />
            <AppBar
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: isOpen,
                })}
                color="primary"
            >
                <Toolbar>
                    <IconButton
                        edge="start"
                        style={{ outline: 'none' }}
                        className={clsx(classes.menuButton)}
                        onClick={() => toggleOpen()}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.navBrand} noWrap>
                        App Information
                    </Typography>
                    <Button color="default">
                        <Link to="/">App</Link>
                    </Button>
                    <Button color="primary">
                        <Link to="/login">Login</Link>
                    </Button>
                    <Button color="secondary">
                        <Link to="/register">register</Link>
                    </Button>
                </Toolbar>
            </AppBar>
            <SideNav isOpen={isOpen} />
        </div>
    )
}

export default withStyles(styles, { withTheme: true })(NavBar)
