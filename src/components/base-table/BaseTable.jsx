import React from 'react'
import {
    makeStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    withStyles,
} from '@material-ui/core'
import { DataGrid } from '@material-ui/data-grid'

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    pagination: {
        marginLeft: 'auto',
    },
}))

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell)

function checkRender({ rows, columns }, classes) {
    if (rows && columns) {
        return (
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        {columns.map((column, index) => (
                            <StyledTableCell key={index} align="center" key={index}>
                                {column}
                            </StyledTableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((entity, index) => (
                        <TableRow key={index}>
                            {Object.keys(entity).map((val, index) => (
                                <TableCell align="center" key={index}>
                                    {entity[val]}
                                </TableCell>
                            ))}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        )
    }
}

export default function BaseTable(props) {
    const classes = useStyles()
    return (
        <TableContainer component={Paper} style={{ marginTop: '30px' }}>
            {checkRender(props, classes)}
        </TableContainer>
    )
}
