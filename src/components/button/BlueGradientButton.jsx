import React from 'react'

import { Button } from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
    root: {
        background: 'linear-gradient(43deg, rgba(128,216,255,1) 0%, rgba(41,98,255,1) 100%)',
        border: 0,
        color: 'white',
        fontWeight: 'blod',
        letterSpacing: '1px',
    },
})

function BlueGradientButton(props) {
    const classes = useStyles()
    const { fullWidth } = props
    return (
        <Button
            className={classes.root}
            fullWidth={fullWidth ? fullWidth : 'false'}
            style={{ outline: 'none' }}
        >
            {props.children}
        </Button>
    )
}

export default BlueGradientButton
