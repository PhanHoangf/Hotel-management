import { makeStyles, withStyles, Button } from '@material-ui/core'
import React from 'react'

const useStyle = makeStyles({
    button: {
        backgroundColor: '#2196f3',
        color: '#0069c0',
        fontWeight: 'bold',
    },
})

function CustomButton(props) {
    const classes = useStyle(props)
    return (
        <Button className={classes.button} style={{ outline: 'none' }}>
            Text info
        </Button>
    )
}

export default CustomButton
