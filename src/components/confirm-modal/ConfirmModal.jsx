import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogTitle from '@material-ui/core/DialogTitle'
import { Button } from '@material-ui/core'
import './confirm-modal.scss'

class ConfirmModal extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="confirm-dialog-container">
                <Dialog open={this.props.state.openModal} aria-labelledby="form-dialog-title">
                    <DialogTitle className="dialog-title text-center">
                        You want to delete this room type?
                    </DialogTitle>
                    <DialogActions>
                        <Button
                            onClick={() => this.props.onHanldeCloseModal(null)}
                            variant="outlined"
                            color="primary"
                            style={{ outline: ' none' }}
                        >
                            CANCEL
                        </Button>
                        <Button
                            onClick={() => {
                                this.props.onHanldeCloseModal(this.props.state.deletedId)
                                this.props.setLoading(true)
                            }}
                            variant="outlined"
                            color="secondary"
                            style={{ outline: ' none' }}
                        >
                            DELETE
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default ConfirmModal
