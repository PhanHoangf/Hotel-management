import React from 'react'
import { connect } from 'react-redux'
import { Redirect, Route } from 'react-router'

function PrivateRoute({ isAuthenticated, children, ...rest }) {
    let auth = isAuthenticated
    return (
        <Route
            {...rest}
            render={({ location }) => {
                return auth ? (
                    children
                ) : (
                    <Redirect to={{ pathname: '/login', state: { from: location } }} />
                )
            }}
        />
    )
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
    }
}

export default connect(mapStateToProps, null)(PrivateRoute)
