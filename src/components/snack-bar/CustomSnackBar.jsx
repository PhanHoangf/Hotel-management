import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import Alert from '@material-ui/lab/Alert'
import { Slide } from '@material-ui/core'

function SlideTransition(props) {
    return <Slide {...props} direction="down" />
}

function CustomSnackBar(props) {
    const { open, message, status } = props

    const handleClose = (event, reason) => {
        props.onHandleCloseSnackbar()
    }

    return (
        <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={open}
            onClose={handleClose}
            autoHideDuration={3000}
            TransitionComponent={SlideTransition}
        >
            <Alert severity={status}>{message}</Alert>
        </Snackbar>
    )
}

export default CustomSnackBar
