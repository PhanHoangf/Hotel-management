import { Backdrop, CircularProgress, makeStyles } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}))

export default function CustomBackdrop(props) {
    const classes = useStyles()
    return (
        <Backdrop open={props.open} className={classes.backdrop}>
            <CircularProgress color="inherit" />
        </Backdrop>
    )
}
