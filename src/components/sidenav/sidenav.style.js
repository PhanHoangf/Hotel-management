import { deepOrange, green } from '@material-ui/core/colors'
const drawerWidth = 240

const styles = (theme) => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        justifyContent: 'space-evenly',
        backgroundColor: '#3F51B5',
    },
    drawerPaper: {
        width: drawerWidth,
    },
    textRight: {
        textAlign: 'right',
    },
    rounded: {
        color: '#fff',
        backgroundColor: green[500],
        padding: '10px 20px 10px 20px',
    },
    brandLogo: {
        width: '50px',
        borderRadius: '5px',
        marginRight: '5px',
    },
    brandText: {
        textTransform: 'uppercase',
        fontWeight: 'bold',
        color: '#fff',
    },
})

export default styles
