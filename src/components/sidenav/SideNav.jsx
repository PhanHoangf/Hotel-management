import React from 'react'

import { Divider, Drawer, Typography, withStyles, Toolbar } from '@material-ui/core'
import styles from './sidenav.style'
import List from '@material-ui/core/List'
import { ReactComponent as LogoSvg } from '../../assets/icons/logo.svg'
import SideNavItems from './sidenav_items/SideNavItems'

function SideNav(props) {
    const { classes, isOpen } = props
    return (
        <Drawer
            className={classes.drawer}
            variant="persistent"
            anchor="left"
            open={isOpen}
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <Toolbar className={classes.drawerHeader} color="primary">
                <LogoSvg className={classes.brandLogo}></LogoSvg>
                <Typography className={classes.brandText}>Hotel Admin</Typography>
            </Toolbar>
            <Divider />
            <List>
                <SideNavItems />
            </List>
        </Drawer>
    )
}

export default withStyles(styles, { withTheme: true })(SideNav)
