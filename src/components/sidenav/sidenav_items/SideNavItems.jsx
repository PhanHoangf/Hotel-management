import React from 'react'
import { ListItem, ListItemIcon, ListItemText, makeStyles } from '@material-ui/core'
import { Link as RouterLink, useRouteMatch } from 'react-router-dom'
import Link from '@material-ui/core/Link'
import { SIDE_NAV_ITEM as sideNavItems } from '../../../constants/PageItems'
import Icon from '@material-ui/core/Icon'

const useStyles = makeStyles(() => ({
    root: {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'space-evenly',
    },
}))

export default function CreateIcon() {
    const { path } = useRouteMatch()
    const classes = useStyles()
    return sideNavItems.map((item) => {
        return (
            <Link
                component={RouterLink}
                to={`${path + item.url}`}
                key={item.id}
                color="inherit"
                underline="none"
                className={classes.link}
                style={{ textDecoration: 'none' }}
            >
                <ListItem button key={`${item.title}`}>
                    <ListItemIcon>
                        <Icon>{item.icon}</Icon>
                    </ListItemIcon>
                    <ListItemText primary={`${item.title}`} />
                </ListItem>
            </Link>
        )
    })
}
