import React from 'react'
import { Chart } from 'react-charts'

export default function LineChart() {
    function randomData() {
        return Math.random() * 10
    }

    const mockData = [
        {
            label: 'Series 1',
            data: [
                { x: 1, y: randomData() },
                { x: 2, y: randomData() },
                { x: 3, y: randomData() },
            ],
        },
        {
            label: 'Series 2',
            data: [
                { x: 1, y: randomData() },
                { x: 2, y: randomData() },
                { x: 3, y: randomData() },
            ],
        },
    ]

    const series = React.useMemo(
        () => ({
            showPoints: true,
        }),
        []
    )
    const data = React.useMemo(
        () =>
            mockData.map((da) => {
                if (da) {
                    return da
                }
            }),
        []
    )

    const axes = React.useMemo(
        () => [
            { primary: true, type: 'linear', position: 'bottom' },
            { type: 'linear', position: 'left' },
        ],
        []
    )

    return (
        <div
            style={{
                width: '100%',
                height: '570px',
            }}
        >
            <Chart data={data} axes={axes} series={series} />
        </div>
    )
}
