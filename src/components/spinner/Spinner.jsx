import React from 'react'
import BounceLoader from 'react-spinners/BounceLoader'
import './spinner.scss'

class Spinner extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true,
        }
    }

    setLoading = (isLoading) => {
        this.setState({ loading: isLoading })
    }

    render() {
        return (
            <div className="sweet-loading">
                <BounceLoader size={150} color={'#123abc'} loading={this.props.isLoading} />
            </div>
        )
    }
}

export default Spinner
