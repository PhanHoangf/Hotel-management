import React, { useState } from 'react'

import { useForm } from 'react-hook-form'
import { Container, Divider, makeStyles, TextField, Button } from '@material-ui/core'

import BaseTable from '../../../components/base-table/BaseTable'

const useStyles = makeStyles(() => ({
    root: {
        height: `85vh`,
        paddingTop: '0',
        paddingBottom: '0',
    },
    inventoryContainer: {
        display: 'flex',
        width: '100%',
        height: '100%',
    },
    leftContainer: {
        flexBasis: '65%',
        padding: '0px 20px 0px 0px',
    },
    rightContainer: {
        borderLeft: '1px solid #E5E5E5',
        display: 'flex',
        flexBasis: '35%',
        padding: '0 0 0px 20px',
    },
    button: {
        marginBottom: '20px',
    },
}))

export default function InventoryContainer(props) {
    const [view, setView] = useState(false)
    const classes = useStyles()
    console.log(props.children)
    return (
        <Container maxWidth="xl" className={classes.root}>
            <div className={classes.inventoryContainer}>
                <div className={classes.leftContainer}>
                    <Button
                        variant="contained"
                        color="secondary"
                        className={classes.button}
                        onClick={() => setView(true)}
                    >
                        Add
                    </Button>
                    {props.children[0]}
                    {props.children[1]}
                </div>
                <div className={classes.rightContainer}>{props.children[2]}</div>
            </div>
        </Container>
    )
}
