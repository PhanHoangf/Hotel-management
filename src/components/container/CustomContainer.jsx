import React from 'react'
// Third library
import { Container, withStyles } from '@material-ui/core'
// --------------------------------------------------//
import formStyle from './form.style'

function CustomContainer(props) {
    const { classes, LoginImage } = props
    return (
        <Container
            maxWidth="xl"
            className={classes.root}
            style={{ backgroundImage: `url(${LoginImage})` }}
        >
            {props.children}
        </Container>
    )
}

export default withStyles(formStyle, { withTheme: true })(CustomContainer)
