import React, { useState } from 'react'
import { addDays } from 'date-fns'
import DateFnsUtils from '@date-io/date-fns'
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import { IconButton, Grid } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { format } from 'date-fns/esm'

export function DatePicker() {
    const [fromDate, setFromdDate] = useState(new Date())
    const [toDate, setToDate] = useState(addDays(fromDate, 1))

    const handleFromDateChange = (date) => {
        setFromdDate(date)
    }

    const handleToDateChange = (date) => {
        setToDate(date)
    }

    const onSearch = () => {
        const newFromDate = format(fromDate, 'MM/dd/yyyy')
        const newToDate = format(toDate, 'MM/dd/yyyy')
        console.log(`From date: ${newFromDate} || To Date: ${newToDate} `)
    }

    return (
        <div>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container justify="flex-start" spacing={3} alignItems="center">
                    <Grid item>
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            label="From"
                            value={fromDate}
                            onChange={handleFromDateChange}
                            minDate={new Date()}
                            maxDate={toDate}
                            format="MM/dd/yyyy"
                        />
                    </Grid>
                    <Grid item>
                        <KeyboardDatePicker
                            disableToolbar
                            variant="inline"
                            label="To"
                            value={toDate}
                            onChange={handleToDateChange}
                            minDate={new Date()}
                            format="MM/dd/yyyy"
                        />
                    </Grid>
                    <Grid item style={{ padding: '0' }}>
                        <IconButton
                            style={{
                                color: '#4CAF50',
                                outline: 'none',
                                marginTop: '23px',
                                fontSize: '15px',
                            }}
                            onClick={onSearch}
                        >
                            <SearchIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </MuiPickersUtilsProvider>
        </div>
    )
}
