import { Card, CardContent, CardMedia, Container, makeStyles } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
})

function SmallCard(props) {
    const classes = useStyles()
    return (
        <Container className={classes.root}>
            <Card>{props.children}</Card>
        </Container>
    )
}

export default SmallCard
