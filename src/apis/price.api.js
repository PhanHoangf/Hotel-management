import { apiService } from './service/apiService'

const roomEndpoints = {
    create: 'room_pricing/create_room_pricing',
    getAll: 'room_pricing',
    delete: 'room_pricing/delete_room_pricing/:id',
    update: 'room_pricing/update_room_pricing/:id',
}

class PriceApi {
    getAllPrices() {
        return apiService.get(roomEndpoints.getAll)
    }
    createPrice(body) {
        return apiService.post(roomEndpoints.create, body)
    }
    deletePrice(id) {
        const newEndpoint = roomEndpoints.delete.replace(':id', id)
        return apiService.delete(newEndpoint)
    }
    updatePrice(id, body) {
        const newEndpoint = roomEndpoints.update.replace(':id', id)
        return apiService.put(newEndpoint, body)
    }
}

export const priceApi = new PriceApi()
