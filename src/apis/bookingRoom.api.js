import { apiService } from './service/apiService'

const bookingRoomEndpoints = {
    create: 'room_booking/create_room_booking',
    getAll: 'room_booking/',
    delete: 'room_booking/delete_room_booking/:id',
    update: 'room_booking/update_room_booking/:id',
}

class BookingRoomApi {
    getAllBookingRooms() {
        return apiService.get(bookingRoomEndpoints.getAll)
    }
    createBookingRoom(body) {
        return apiService.post(bookingRoomEndpoints.create, body)
    }
    deleteBookingRoom(id) {
        const newEndpoint = bookingRoomEndpoints.delete.replace(':id', id)
        return apiService.delete(newEndpoint)
    }
    updateBookingRoom(id, body) {
        const newEndpoint = bookingRoomEndpoints.update.replace(':id', id)
        console.log('put')
        return apiService.put(newEndpoint, body)
    }
}

export const bookingRoomApi = new BookingRoomApi()
