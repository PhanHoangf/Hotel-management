import { apiService } from './service/apiService'

const roomEndpoints = {
    create: 'room/create_room',
    getAll: 'room/',
    delete: 'room/delete_room/:id',
    update: 'room/update_room/:id',
    cleanRoom: 'room/clean_room/:id',
}

class RoomApi {
    getAllRooms() {
        return apiService.get(roomEndpoints.getAll)
    }
    createRoom(body) {
        return apiService.post(roomEndpoints.create, body)
    }
    deleteRoom(id) {
        const newEndpoint = roomEndpoints.delete.replace(':id', id)
        return apiService.delete(newEndpoint)
    }
    updateRoom(id, body) {
        const newEndpoint = roomEndpoints.update.replace(':id', id)
        return apiService.put(newEndpoint, body)
    }
    cleanRoom(id, body) {
        const newEndpoint = roomEndpoints.cleanRoom.replace(':id', id)
        return apiService.put(newEndpoint, body)
    }
}

export const roomApi = new RoomApi()
