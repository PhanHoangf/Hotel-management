import axios from 'axios'
import { environment } from '../../environment'

export default class ApiService {
    constructor() {
        this.http = axios.create({
            baseURL: `${environment.baseUrl}`,
        })

        this.http.interceptors.request.use((req) => {
            this._setAuthorization(req)
            return req
        })
    }

    get(endpoint, params = {}) {
        return this.http.get(endpoint, params).catch(this._handleError)
    }

    post(endpoint, body) {
        return this.http.post(endpoint, body).catch(this._handleError)
    }

    put(endpoint, body) {
        return this.http.put(endpoint, body).catch(this._handleError)
    }

    delete(endpoint) {
        return this.http.delete(endpoint).catch(this._handleError)
    }

    _handleError(error) {
        console.log(error)
    }

    _setAuthorization(req) {
        // console.log('a')
        const token = localStorage.getItem('token')
        if (token) {
            req.headers['authorization'] = `Bearer ${token}`
        }
        // return req
    }
}

export const apiService = new ApiService()
