import { apiService } from './service/apiService'

export const userEndpoints = {
    registerUser: 'users/register',
    login: 'users/login',
    logout: 'users/logout',
    getMe: 'users/me',
}
// const TEST_LOGIN_URL = 'http://localhost:3000/api/auth/login'
// const apiService = new ApiService()

class UserApi {
    getMe() {
        return apiService.get(userEndpoints.getMe)
    }

    login(body) {
        return apiService.post(userEndpoints.login, body)
    }

    register(body) {
        return apiService.post(userEndpoints.registerUser, body)
    }

    logout() {
        localStorage.removeItem('token')
    }

    setToken(token) {
        localStorage.setItem('token', token)
    }
}

export const userApi = new UserApi()
