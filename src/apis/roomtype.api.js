import { apiService } from './service/apiService'

const roomTypeEndpoints = {
    create: 'type_room/create_room',
    getAll: 'type_room/',
    delete: 'type_room/delete_room/:id',
    update: 'type_room/update_room/:id',
}

class RoomTypeApi {
    getAllRoomType() {
        return apiService.get(roomTypeEndpoints.getAll)
    }
    createRoomType(body) {
        return apiService.post(roomTypeEndpoints.create, body)
    }
    deleteRoomType(id) {
        const newEndpoint = roomTypeEndpoints.delete.replace(':id', id)
        return apiService.delete(newEndpoint)
    }
    updateRoomType(id, body) {
        const newEndpoint = roomTypeEndpoints.update.replace(':id', id)
        return apiService.put(newEndpoint, body)
    }
}

export const roomTypeApi = new RoomTypeApi()
