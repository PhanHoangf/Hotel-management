import React from 'react'
import App from './app/App'
import ReactDOM from 'react-dom'
import './index.scss'
import { storeRedux } from './redux/store'
import { Provider } from 'react-redux'

ReactDOM.render(
    <Provider store={storeRedux}>
        <App />
    </Provider>,
    document.getElementById('root')
)
