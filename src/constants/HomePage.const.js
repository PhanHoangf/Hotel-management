export const HomePageConst = [
    {
        id: 1,
        icon: 'cached',
        title: 'Rent for day',
        subtitle: 'Update',
        subIcon: 'cached',
        unit: 'turn',
        color: '#F3BB45',
    },
    {
        id: 2,
        icon: 'meeting_room',
        title: 'Waiting room',
        subtitle: 'Rent room',
        subIcon: 'vpn_key',
        unit: 'rooms',
        color: '#3F9789',
    },

    {
        id: 3,
        icon: 'sensor_door',
        title: 'Rented room',
        subtitle: 'Menu & checkout',
        subIcon: 'reply',
        unit: 'rooms',
        color: '#F25531',
    },
    {
        id: 4,
        icon: 'cleaning_services',
        title: 'Cleaning room',
        subtitle: 'Clean room',
        subIcon: 'cleaning_services',
        unit: 'rooms',
        color: '#F25531',
    },
]
export const MenuConst = [
    {
        id: 1,
        icon: 'list',
        title: 'All',
    },
    {
        id: 2,
        icon: 'vpn_key',
        title: 'Receive room',
    },
    {
        id: 3,
        icon: 'undo',
        title: 'Return room',
    },
    {
        id: 4,
        icon: 'done',
        title: 'Clean room',
    },
    {
        id: 5,
        icon: 'loyalty',
        title: 'Require clean room',
    },
    {
        id: 6,
        icon: 'delete',
        title: 'Delete data rent room',
    },
]
