import { v4 as uuidv4 } from 'uuid'

export const INVENTORY = [
    {
        id: uuidv4(),
        title: 'Storage Status',
        url: 'status',
        icon: 'pie_chart',
        color: '#03A9F4',
    },
    {
        id: uuidv4(),
        title: 'Add',
        url: 'add',
        icon: 'get_app',
        color: '#4CAF50',
    },
    {
        id: uuidv4(),
        title: 'Update',
        url: 'update',
        icon: 'vertical_align_center',
        color: '#FF5722',
    },
    {
        id: uuidv4(),
        title: 'Check Storage',
        url: 'check',
        icon: 'beenhere',
        color: '#F44336',
    },
]

export const SIDE_NAV_ITEM = [
    {
        id: uuidv4(),
        title: 'Home',
        url: '/home',
        icon: 'home',
    },
    {
        id: uuidv4(),
        title: 'Check',
        url: '/check-in',
        icon: 'assignment_turned_in',
    },
    {
        id: uuidv4(),
        title: 'Schedule',
        url: '/schedule',
        icon: 'schedule',
    },
    {
        id: uuidv4(),
        title: 'Storage',
        url: '/inventory',
        icon: 'storage',
    },
    {
        id: uuidv4(),
        title: 'Money',
        url: '/money',
        icon: 'money',
    },
    {
        id: uuidv4(),
        title: 'Setting',
        url: '/setting',
        icon: 'settings',
    },
    {
        id: uuidv4(),
        title: 'Acount',
        url: '/account',
        icon: 'account_circle',
    },
    {
        id: uuidv4(),
        title: 'Payment',
        url: '/payment',
        icon: 'payment',
    },
    {
        id: uuidv4(),
        title: 'Logout',
        icon: 'login',
    },
]

const SETTING_ICON_COLOR = '#03A9F4'

export const SETTING_ITEMS = [
    {
        id: uuidv4(),
        title: 'Payment',
        url: 'rule',
        icon: 'memory',
        color: SETTING_ICON_COLOR,
    },
    {
        id: uuidv4(),
        title: 'Room types',
        url: 'roomtype',
        icon: 'home',
        color: SETTING_ICON_COLOR,
    },
    {
        id: uuidv4(),
        title: 'Room',
        url: 'room',
        icon: 'hotel',
        color: SETTING_ICON_COLOR,
    },
    {
        id: uuidv4(),
        title: 'Menu',
        url: 'menu',
        icon: 'menu_book',
        color: SETTING_ICON_COLOR,
    },
    {
        id: uuidv4(),
        title: 'Sub account',
        url: 'usersub',
        icon: 'account_tree',
        color: SETTING_ICON_COLOR,
    },
    {
        id: uuidv4(),
        title: 'Statistic',
        url: 'report',
        icon: 'insert_chart_outlined',
        color: SETTING_ICON_COLOR,
    },
    {
        id: uuidv4(),
        title: 'History',
        url: 'history',
        icon: 'description',
        color: SETTING_ICON_COLOR,
    },
    {
        id: uuidv4(),
        title: 'Other setting',
        url: 'othersetting',
        icon: 'perm_data_setting',
        color: SETTING_ICON_COLOR,
    },
]
