export const LANDING_PAGE_CONST = [
    {
        title: 'Home',
        link: '/',
    },
    {
        title: 'Features',
        link: '/features',
    },
    {
        title: 'Prices',
        link: '/prices',
    },
    {
        title: 'Fag',
        link: '/fag',
    },
    {
        title: 'Hotline',
        link: '/hotline',
    },
    {
        title: 'SignIn',
        link: '/signin',
    },
]
